# Curso de CSS

- Flexbox

### Flexbox

#### Intro

- El layout flexbox es un modulo de CSS (un conjunto de propiedades con sus respectivos valores) relativamente nuevo pero con muy buen soporte que proporciona una manera eficiente de definir como van ocupar el espacio interno de un contenedor sus hijos sin que sea necesario saber el tamaño del contenedor, de ahí el nombre flex o fluído.
- Cuando definimos un contenedor como `display: flex` podremos, a través de una serie de propiedades de CSS, alterar el ancho y alto de sus hijos para acomodarlos en el espacio dispoñible.
- Flexbox es complementario al modulo de Grid, y, mientras que este último se usa normalmente para definir layouts multidimensionales, el flexbox es perfecto para definir layouts más simples.

### Terminología

![](./flex-es.png)

- **Contenedor**: es el elemento que se define como *flex* y por lo tanto sus hijos (directos) van a ser afectados por este tipo de display. La mayoría de propiedades CSS del módulo flexbox se aplican al contenedor.
- **Hijos**: son los elementos internos de primer nivel del contenedor que se distribuyen en el espacio según las propiedades aplicadas al elemento padre. También hay una serie de propiedades en modulo Flexbox que se aplican a los hijos.
- **Eje horizontal**: es una línea imaginaria que cruza horizontalmente el contenedor y sobre esa línea se distribuirán los hijos de los elementos flex de direccion horizontal.
- **Eje vertical**: lo mismo que el anterior pero en vertical.

#### Propiedades del contenedor

Para que un contenedor sea de tipo flex tiene que tener la propiedad de CSS `display: flex`. Todos los hijos del contenedor que tenga esa propiedad estarán afectados por las propiedades de flexbox que vemos a continuación.

- **flex-direction:** determina sobre cual de los ejes van a fluir los hijos. Los posibles valores son:
- `row`: horizontalmente de izquierda a derecha _(valor por defecto)_
  - `row-reverse`: horizontalmente de derecha a izquierda
  - `column`: verticalmente de arriba a abajo
  - `column-reverse`: verticalmente de abajo a arriba.
  
- **flex-wrap**: por defecto flexbox va a intentar distribuir todos los hijos en una sola línea aún que se pase del ancho del contenedor. En caso de que queramos que los hijos que non encajen pasen a la siguiente línea tenemos que usar esta propiedad. Los valores posibles son:
- `nowrap`: todos los hijos van estar en una sola línea _(valor por defecto)_
  - `wrap`: los hijos pasarán a la línea siguiente en caso de que no encajen en la anterior.
  - `wrap-reverse`: lo mismo que la anterior pero pasarán a la línea anterior.
  
- **justify-content**: esta propiedad define como se van alinear los hijos en el eje que tengamos escogido con `flex-direction`. Los valores posibles son:
  - `flex-start`: los hijos estarán apilados al principio de la línea definida por el eje. *(valor por defecto)*
  - `flex-end`: los hijos se apilarán al final de la línea.
  - `center`: los hijos se apilarán en el centro de la línea.
  - `space-between`: los hijos se distribuirán ocupando toda la línea correspondiente estando el primeiro pegado al principio de la línea y el último al final de la misma.
  - `space-around`: similar al anterior pero el primero y el último hijo tienen una unidad de separación con respecto al principio y al final respectivamente, y dos unidades de separación entre el resto de los hijos.
  - `space-evenly`: hace que todas las separaciones incluídas la inicial e la final sean iguales.
- **align-items:** esta propiedad es similar a la anterior pero se aplica a la distribución en el eje contrario al que tengamos escogido con `flex-direction`. Los posibles valores son:
  - `flex-start, flex-end, center`: igual que en el caso de `justify-content`.
  - `baseline`: alinea los hijos según la línea base del texto (se usa poco).
  - `stretch:` estira los elementos para ocupar todo el espacio del contenedor _(valor por decto)_
- **align-content:** define como se van a comportar las líneas de elementos que crea flex:
  - `flex-start`: apila las líneas del grupo de hijos al principio del eje.
  - `flex-end`: lo mismo pero al final del eje.
  - `center, space-between, space-around`: apila las líneas de la misma forma que lo hacen estas mismos valores de la propiedad `justify-content` .
  - `stretch`: hace que las líneas ocupen todo el espacio del contenedor _(valor por defecto)_

#### Propiedades de los hijos

- **order:** determina el orden en el que van aparecer los hijos distribuídos por flex independientemente del orden en que aparezcan en el HTML. Valores posibles:
  - `número entero`. _(valor por defecto: 0)_
- **flex-grow:** determina si un hijo puede aumentar su ancho o alto para ocupar el espazo libre disponible en el eje correspondiente. Valores posibles:
  - `número entero`. (valor por defecto: 0)
- **flex-shrink:** lo mismo que el anterior pero en lugar de aumentar sería diminuír si los hijos ocupan más espacio del disponible en el eje del contenedor.
- **flex-basis:** determina el tamaño por defecto del hijo antes de que flex distribuya el espacio. Valores por defecto.
  - `un tamaño determinado por cualquier unidad de css (em, rem, %, px, etc...) o auto.` _(valor por defecto: auto)_
- **align-self:** permite establecer un valor propio do hijo que se aplicaría en lugar del valor asignado por el `align-items` definido en el  contenedor.
  - `auto, flex-start, flex-end, center, baseline o stretch.`
