# Curso de CSS

- Grid 1

## CSS Grid Layout 1

- **CSS Grid Layout** es un módulo de CSS que permite dividir un elemento en filas y columnas creando una cuadrícula de 2 dimensiones, a diferencia de Flexbox que es un sistema unidimensional.
- Trabajaremos con Grid asignando una serie de propiedades CSS al contedor (_grid container_) y a los hijos (_grid items_).

### Terminología

- _Grid container_: es el elemento en el que queremos crear la cuadrícula y que se dividirá en filas y columnas.
- _Grid item_: cada uno de los hijos de primer nivel del _grid container_ y que podremos posicionar en la cuadrícula.
- _Grid line_: las líneas que separan las filas y columnas determinando la estructura de la cuadrícula.
- _Grid track_: cada una de las filas o columnas que crea la cuadrícula
- _Grid cell_: cada una de las celdas que crean las filas y columnas al cruzarse, se trata del elemento mínimo de la cuadrícula.
- _Grid area_: un grupo de celdas en el que podremos posicionar un ítem.

### Definiendo filas y columnas

- Antes de nada, para crear una cuadrícula en un elemento tenemos que aplicar la declaración CSS `display: grid`.
- A diferencia de flexbox la aplicación desta propiedad non modifica nada en nuestro layout ya que antes tenemos que definir las columnas y/o filas que va a tener.
- Para determinar las filas y columnas usamos las propiedades `grid-template-columns`y `grid-template-rows` que definirán los _tracks_:

```css
.container {
  display: grid;
  grid-template-columns: 500px 20rem;
  grid-template-rows: 100px 100px 100px;
}
```

- En el ejemplo anterior estamos creando una cuadrícula de 2 columnas y 3 filas.

- Podemos establecer el tamaño de las filas y columnas en cualquier unidad válida de CSS.

- También podremos usar las unidades especiales:

  - _auto_: primeiro colocará todas las filas o columnas que tengan definido un tamaño y después llenará el espacio sobrante hasta llenar el contenedor con las filas/columnas que tengan tamaño auto siendo el tamaño mínimo el determinado por el contenido de las mismas.
  - _fr_: es una unidad que permite especificar el tamaño en base al espacio libre que quede en la fila/columna, todas as filas/columnas que tengan tamaño en fr se dividirán el espacio en base a su valor. Funciona de forma similar al _flex-grow / flex-shrink_.

- Podemos usar también la función _repeat_ para definir los tracks:

  ```css
  .container {
    display: grid;
    grid-template-columns: repeat(4, 100px);
  	/* Esto creará 4 columnas de 100px */

  	grid-template-rows: repeat(3, 10rem 5rem);
  	/* Esto creará: 10rem 5rem 10rem 5rem 10rem 5rem /*
  }
  ```

#### Tracks explícitos e implícitos

- Por ejemplo: creamos una cuadrícula de 3 columnas y dos filas:

  ```css
  .container {
    display: grid;
    grid-template-columns: 10rem 1fr 15rem;
    grid-template-rows: 100px 100px;
  }
  ```

  Si el contenedor tiene 10 hijos de primer nivel, los primeros 6 hijos se colocaraán en tres columnas y dos filas y al acabar las columnas y filas definidas los siguientes hijos pasarán a la siguiente línea y así sucesivamente.

- Las filas y columnas definidas con `grid-template-rows`y `grid-template-columns` se llaman **grid explícito**. El resto de las filas/columnas son tracks implícitas. El tamaño de los tracks implícitos podremos definirlos usando las propiedades `grid-auto-rows`e `grid-auto-columns`:

  ```css
  .container {
    display: grid;
    grid-template-columns: 10rem 1fr 15rem;
    grid-template-rows: 100px 100px;
    grid-auto-rows: 20px 50px;
  }
  ```

  En este caso las filas que no son explícitas tendrán una altura de 20px a primera, tercera, quinta, etc. y 50px la segunda, cuarta, sexta, etc...

- Si definimos `grid-auto-columns` la mayoría de las veces no pasará nada porque por defecto las cuadrículas van crecer en filas y no en columnas. Pero podremos cambiar este comportamiento con la propiedad `grid-auto-flow` que por defecto tiene el valor _row_ pero podremos establecerlo como _column_ y así la cuadrícula en lugar de crecer en filas crecerá en columnas. No se usa mucho ya que la mayoría de los layouts con los que trabajaremos crecen verticalmente y no horizontalmente.

### Definiendo el tamaño de los items

- Por defecto cada _grid item_ ocupara un _grid cell_ (ver termilogía) pero podremos modificar este comportamiento definiendo para cada item que número de celdas queremos que ocupe, y también cuales serán estas.

- Aún que definamos explícitamente el tamaño de cada _track_, en última instancia si definimos un tamaño fijo (con width/height) para un hijo ou bien su contenido indivisible (imágenes, palabras palabras largas) van a producir una modificación de toda la cuadrícula.

- Para prevenir esto podemos asignar las propiedades `grid-column` y/o `grid-row` a cualquier de los grid items para que estos ocupen más de una celda:

  ```css
  .container {
    grid-template-columns: 10rem 1fr 15rem;
    grid-template-rows: 100px 100px;
  }

  .item1 {
    grid-column: span 2;
    /* Esto hará que el hijo .item1 se expanda en 2 columnas en lugar de 1 */
  }

  .item2 {
    grid-column: 1 / 3;
    /* O .item2 comenzará en la columna 1 y se expandirá hasta la 3 */
  }

  .item3 {
    grid-column: 2 / -1;
    /* El item3 empezará en la columna 2 y llegará hasta la última (la última es la -1, la anterior a la última -2, etc...) */
  }

  .item4 {
    grid-column: 2 / -2;
    /* El item4 empezará en la columna 2 y llegará hasta la penúltima */
  }

  .item5 {
    grid-column: 2 / span 3;
    /* El .item5 empezara en la columna 2 y se expandriá 3 columnas */
  }
  ```

- Si no hay espacio suficiente en el track puede pasar que no encajen el resto de los ítems y aparezcan espacios vacíos al pasar los que no entren a la siguiente línea o columna.

- Si hacemos que un elemento se expanda (por ejemplo) más columnas de las que hay en el track empezará a crear columnas implícitas.

### auto-fill y auto-fit

- Si queremos crear una cuadrícula de filas/columnas variables en base al tamaño de los _items_ podemos usar los keywords _auto-fill o auto-fit_ cuando definamos la cuadrícula.

  ```css
  .container {
    display: grid;
    grid-template-columns: repeat(auto-fill, 150px);
  }
  ```

  En este caso creará tantas columnas de 150px de ancho como permita el ancho del contedor, las que no encajen pasarán automáticamente a la siguiente línea.

- La diferencia es:

  - auto-fill: va a llenar la fila hasta el final con un número de columnas del tamaño especificado aún que no existan items suficientes para llenarla.
  - auto-fit: sólo creará las columnas necesarias para el número de ítems.

### minmax

- La combinación de `auto-fill y auto-fit` con `minmax` nos va a permitir crear cuadrículas `responsive` sin necesidad de media queries.

- `minmax nos va a permitir definir el ancho mínimo y máximo de las filas/columnas.

  ```css
  .container {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
  }
  ```

  En este ejemplo creará una fila con tantas columnas como pueda siendo el tamaño de la columna mínimo de 150px y máximo de todo el espacio disponible en la fila.

### Grid areas

- Podemos darle nombres a agrupaciones de celdas creadas por las filas y columnas para referirnos a ellas a la hora de posicionar _items_.

- Por ejemplo creamos un grid de 3 columnas y 3 filas:

  ```css
  .container {
    display: grid;
    grid-gap: 1rem;
    grid-template-columns: 1fr 600px 1fr;
    grid-template-rows: 200px 200px 100px;
    grid-template-areas:
      'lateral-1 content lateral-2'
      'lateral-1 content lateral-2'
      'footer footer footer';
  }
  
  footer {
    grid-area: footer;
  }
  
  .content {
    grid-area: content;
  }
  ```

  Con `grid-template-areas` agrupamos por nombre las celdas creadas por los tracks y podemos posicionar en ellas los items de la cuadrícula.

### Alineando la cuadrícula y sus ítems

#### Ítems

- Por defecto los _grid items_ ocupan todo el espacio determinado por el tamaño de la celda en la que están posicionados.
- De forma similar a flexbox podremos modificar que espacio ocuparán usando las propiedades _justify-items e align-items_ que se aplican al contenedor.
- La propiedad _justify-items_ por defecto tiene un valor de _stretch_ que hará que os îtems ocupen todo el espacio horizonal de la celda. También podremos darle los valores: start, center e end para posicionar al principio, centro y final de la celda respectivamente.
- La propiedad _align-items_ tiene un comportamento exactamente igual pero siempre se aplica verticalmente.
- Podemos aplicar estas dos propiedades al mismo tiempo usando _place-items_:

```css
.container {
  place-items: center center;
  /* Esto es lo mismo que justify-items: center y align-items: center */
}
```

- Si queremos aplicar una de estas propiedades a un item específico y non a todos, podemos usar las propiedades _justify-self y align-self_ en ese item (no en el contendor). Estas propiedades permiten los mismos valores que las anteriormente comentadas.

#### Cuadrícula

- Si la cuadrícula no ocupa todo el espacio del contenedor porque la suma de sus columnas/filas es menor que el ancho/alto del contenedor podremos usar las propiedadess _justify-content y align-content_ para hacer que los tracks de la cuadrícula se distribuyan mejor en el espacio determinado por el contenedor.
- La propiedad justify-content distribuirá el espacio horizontal de las columnas. El valor por defecto es *start* que posicionará las columnas al principio del contenedor, pero podremos usar:
  - *center*: para posicionarlas en el centro del contenedor
  - _end_: al final del contenedor
  - _stretch_: estirando el ancho de las columnas para ocupar todo o espacio disponible
  - _space-around, space-between, space-evenly_: tienen un comportamiento igual que en flexbox.
- La propiedad _align-content_ tiene un comportamiento exactamente igual pero se aplica verticalmente.
- Podemos aplicar estas dos propiedades al mismo tiempo usando _place-content_ igual que en el ejemplo anterior.