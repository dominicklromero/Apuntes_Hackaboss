# Curso de CSS

- Unidades y colores
- Estilo de textos
- Tipografía
- El modelo de cajas
- Estilo de cajas

### Unidades y cores

#### Unidades

- Muchas de las propiedades de  CSS que venimos hasta ahora y muchas de las que veremos tienen valores numéricos, la mayoría de ellos vano asociados a unidades pero no todos.

- Las unidades historicamente más habituales son los píxeles (px), que son unidades absolutas y siempre miden lo mismo:

  ```css
  p {
    margin: 5px;
    padding: 10px;
    border: 2px solid black;
  }
  ```

- Hay otras unidades absolutas como cuartos de milimetro (q), milimetros (mm), centímetros (cm), pulgadas (in), etc... pero suuso no es habitual.

- Con la proliferación de la web en dispositivos de diferentes tamaños de pantalla las unidades absolutas están quedando cada vez más en desuso a favor de las relativas que en general dependen de otros factores como el tamaño del contenedor u otros factores proporcionales:

  - **em**: `1 em` coincide con el tamaño de la fuente del elemento donde se aplica, que normalmente ven heredada del elemento padre. Si no se hace ningún cambio de fuente el valor por defecto de `1 em` nos navegadores es de 16 px.
  - **rem**: muy similar al anterior pero en lugar de tener en cuenta el tamaño de la fuente heredada del elemento padre siempre coge el tamaño de fuente del elemento raíz que es el `html`. Por lo que se usamos siempre unidades rem será muy sencillo modificar la proporción simplemente modificando el tamaño de fuente del elemento `html` mediante  CSS.
  - **vw, vh**: unidades que dependen del tamaño de la  ventana del navegador. `1 vw` es una centésima parte del ancho de la  ventana y `1 vh` es una centésima parte del alto de la  ventana. Son útiles para establecer tamaños de cajas como veremos más adelante.
  - **porcentajes**: si establecemos una unidad como porcentaje estamos estableciendo el porcentaje del valor por defecto de esa propiedad. Por ejemplo ` font-size: 200%;` establecería un tamaño de fuente  doble de la heredada para ese elemento y se usamos ` width: 50%;` me los establecería un ancho de la mitad del contenedor para ese elemento.

- Si tenemos que establecer una unidad con valor 0 no es necesario escribir las unidades, Cero siempre es cero usemos la unidad que usemos: ` margin: 0;` por ejemplo.

- Hay otras propiedades que admiten valores sin unidades, por ejemplo `line-height` que establece el alto de línea de un texto. Por defecto es 1.2, sin unidades.

#### Colores

- Hay varias maneras de especificar los colores en  CSS y todas tienen el mismo resultado..

- El modelo de color de los dispositivos modernos es de 24 bits por lo que admite 16.7 millones de colores posibles,  estos son una combinación de 3 canales de color: rojo (R), verde (G) y azul (B). Cada una de estas unidades permite 256 valores numéricos, entre 0 y 255.

- Maneras de especificar colores en CSS:

  - Cores con nombre: CSS soporta un gran número de colores por defecto que tienen un nome asociado. Aquí podéis ver la lista completa: https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords Para establecer un color con nombre simplemente escribimos el nombre del color:

    ```css
    h1 {
      color: rebeccapurple;
    }
    ```

  - Formato hexadecimal: es una notación de color que permite establecer el valor de los 3 canales como 6 números hexadecimales (valores entre 0 y f: 0123456789abcdef) agrupados en pares. Es una notación bastante común y todos los programas de edición de gráficos permiten ver el valor hexadecimal de cualquier color:

    ```css
    h1 {
      color: #663399;
    }
    ```

  - Formato RGB: es una manera más moderna de establecer el color en el que simplemente indicamos los valores de cada canal RGB en formato decimal (entre 0 y 255), se usa así:

    ```css
    h1 {
      color: rgb(102, 51, 153);
    }
    ```

  - El formato  RGB permite establecer un cuarto valor que especifica la opacidad o transparencia del color, el valor va entre 0 donde el color sería completamente transparente y 1 donde sería un color sólido (igual que se no se había establecido). Los valores intermedios establecerían diferentes grados de transparencia,  p.y. 0.5 sería un 50% de transparencia. Se usa  rgba en lugar de  rgb:

    ```css
    h1 {
      color: rgba(102, 51, 153, 0.5);
    }
    ```

- Usando esta utilidad online podemos cambiar entre formatos de color: https://serennu.com/colour/hsltorgb.php

### Estilos de texto

- Muchos de los elementos HTML que vimos hasta agora pueden contener texto y podemos darle estilos visuales a ese texto mediante CSS: colores, tipografías, tamaños, alineación, etc...
- En general los estilos se agrupan en dos tipos: estilos de fuente y estilos de disposición.

#### Estilos de fuente

- Podemos darle un color a la fuente usando la propiedade `color`:

  ```css
  p {
    color: red;
  }
  ```

- Podemos asignarle una tipografía con `font-family`:

  ```css
  p {
    font-family: Arial;
  }
  ```

  - Las tipografías pueden establecerse usando valores genéricos: sans-serif,  monospace y serif serían los valores genéricos más habituales para fuentes sin serifa,  monoespaciadas y con  serifa. Especificando estos valores dejamos al navegador que use la tipografía que considere más apropiada para ese valor.

  - También podemos establecer valores específicos usando el nombre de la tipografía, por ejemplo: Arial. Si el nombre de la tipografía tiene varias palabras debemos ponerlo entre comillas (""):

    ```css
    p {
      font-family: 'Comic Sans MS';
    }
    ```

  - Es normal especificar varias tipografías separadas por comas y el navegador cogerá a primeira que tenga disponible:

    ```css
    p {
      font-family: 'Trebuchet MS', Arial, sans-serif;
    }
    ```

  - Es recomendable en este caso é establecer como última opción un nombre genérico para que en el caso de que non tenga dispoñible ninguno de los tipos anteriores sempre coja algunha similar genérica.

- Para darle tamaño al texto usamos la propiedad `font-size` especificada en cualquiera de las unidades que vimos anteriormente:

  ```css
  p {
    font-size: 200%;
  }

  p {
    font-size: 2rem;
  }

  p {
    font-size: 20px;
  }
  ```

- Hay otras propiedades como:

  - [font-style](https://developer.mozilla.org/en-US/docs/Web/CSS/font-style): permite establecer texto en cursiva.
  - [font-weight](https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight): permite establecer el grado de grosor del texto.
  - [text-transform](https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform): permite modificar la capitalización del texto.
  - [text-decoration](https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration): permite establecer diferentes tipos de subrayados.

- Se queremos darle una sombra al texto usamos la propiedad `text-shadow` que define una sombra detrás del mismo establecendo el desplazamiento horizontal, desplazamiento vertical, grado de difusión y color de la sombra:

  ```css
  h1 {
    text-shadow: 2px 2px 5px black;
  }
  ```

- El ejemplo anterior crearía una sombra desplazada 2 pixeles horizontal y verticalmente con respecto al texto y con un radio de difusión de 5px. El color de la sombra sería negro.

#### Estilos de disposición

- La disposición del texto afecta a la alineación, interlineado y separación de letras y palabras.

- Para cambiar el alineamiento del texto usamos la propiedad ` text-align` que admite los valores tradicionales ` left, center y right`. También permite el valor  `justify` para justificar el texto a los  bordes pero esto último no es recomendado por las limitaciones de los  navegagores a la hora de dividir palabras.

  ```css
  h1 {
    text-align: center;
  }
  ```

* Para cambiar el alto de línea usamos _line-height_ que admite valores **sin** unidad. El valor por defecto es 1.2. Por exemplo para poñer interlineado a doble espacio:

  ```css
  p {
    line-height: 2;
  }
  ```

* La separación entre letras y palabras se puede controlar con las propiedades *letter-spacing* y word-spacing que permiten valores positivos o negativos en cualquiera de las unidades conocidas e funcionan de forma similar:

  ```css
  h1 {
    letter-spacing: -2px;
    word-spacing: 0.3rem;
  }
  ```

### Tipografías

- Hasta ahora aprendimos a asignar tipografías al texto usando `font-family`, pero siempre dependíamos de las tipografías que tuviera instaladas o sistema operativo donde se visaualizaba la web.

- Los navegadores modernos permiten usar tipografías aún que non estén disponibles en sistema operativo transmitiendo el propio archivo de la tipografía.

- El método más habitual es usar Google Fonts, sólo temos que escoger la fuente y nos mostrará el código que tenemos que introducir en nuestro archivo CSS para usar ese tipo, por ejemplo, para la tipografía [Open Sans](https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans) isto sería:

  ```css
  @import url('https://fonts.googleapis.com/css?family=Open+Sans');
  ```

  Este código me los tendría que colocarlo al principio de todo del  CSS ya que es un `@import`, después de hacer eso podríamos usar esta tipografía de esta forma:

  ```css
  p {
    font-family: 'Open Sans', sans-serif;
  }
  ```

  sempre especificamso un nombre de tipografía genérico al final por si hubiera algún problema cargando la tipografía de Google.

- En caso de que Google  Fonts no tenga la tipografía que queremos tenemos que conseguir el archivo de la tipografía original.

- Después me los tendría que  convertir esa tipografía a varios formatos, ya que no todos los navegadores soportan los mismos formatos de fuente. Los navegadores modernos soportan los formatos _woff_ y _woff2_ por lo que usando esos llegaría si no tenemos necesidades especiales.

- Para hacer la conversión podemos usar un servicio web como este: https://www.fontsquirrel.com/tools/webfont-generator

- Estos servicios aparte de convertir la fuente a los formatos más habituales también nos proporcionan el código a insertar o importar en nuestro fichero CSS.

- Después de hacer la conversión tenemos que colocar los archivos resultantes en una carpeta en nuestro sitio web y tenemos que definir la fuente a usar mediante el statement `@font-face`:

  ```css
  @font-face {
    
    font-family: 'cantarellregular';
    src: url('cantarell-regular-webfont.woff2') for mat('woff2'), url('cantarell-regular-webfont.woff')
        format('woff');
    font-weight: normal;
    font-style: normal;
  }
  ```

- Es importante fijarnos que las referencias a `url(...)`  apunten correctamente a los ficheros en nuestro servidor.


### O modelo de caixas

- El modelo de cajas de  CSS es la base de la creación de  layouts en las webs. Todos los elementos que creamos están representados por una caja que tiene una serie de propiedades:

  ![img](https://mdn.mozillademos.org/files/13647/box-model-standard-small.png)

- Ancho y alto:

  - Las propiedades ` width` y ` height` definen el ancho y alto de las cajas respectivamente y pueden ser definidas en cualquiera de las unidades que vimos hasta ahora. Excepto `height` que no puede ser definido en unidades de tanto por ciento (%).
  - El valor por defecto es `auto`. Cuando tienen ese valor es el navegador quien calcula el tamaño de las cajas que por defecto es el 100% del ancho de la caja padre y el alto establecido por los contenidos internos de la caja.

- Padding:

  - La propiedad `padding` determina el margen interno de la caja; la distancia entre los contenidos y el borde interno.
  - Podemos especificar padding de cada uno de los lados de la caja con las propiedades `padding-top, padding-right, padding-bottom e padding-left`.

- Margin:

  - La propiedad margin determina el márgen externo de la caja: la distancia de los bordes de la caja con otras cajas que la rodean.
  - El tamaño del margen podemos establecerlo en cualquiera de las unidades de tamaño que ya conocemos.
  - Si establecemos el valor como `auto` el navegador cogerá el mayor márgen disponible. Esto es aplicable a los márgenes laterales y sirve tradicionalmente para centrar un elemento en un contedor.
  - Al igual que el `padding` podemos especificar el márgen para cada lado usando: `margin-top, margin-right, margin-bottom e margin-left`.
  - Los márgenes tienen un comportamiento especial que se llama _margin collapsing_: cuando dos cajas con márgen se tocan la distancia entre ellas no es la suma de los márgenes, es la distancia definida por el márgen más grande.

- Borde:

  - La propiedad `border` define el ancho, estilo y cor del borde de la caja: la línea que rodea a la caja. Veremos más adelante como definirlo.

- De todo esto se concluye que el ancho total de una caja es la suma do su ancho más el padding de cada lado más el borde de cada lado. Si queremos cambiar este comportamiento por defecto podemos usar la propiedad `box-sizing` que permite los siguientes valores:

  - `content-box`: valor por defecto. Si establecemos un `width` da caja de 100px para calcular el ancho total el navegador sumará el borde y el padding.
  - `border-box`: cambia el modelo de cajas completamente. Si establecemos un ancho de 100px el ancho total siempre será 100px independentemente del ancho del borde y del padding.

#### Overflow

- Al poder establecer un ancho y alto de las cajas manualmente puede  ocurrir que en algunos casos el contenido no entre dentro de la caja, para establecer que pasa con ese contenido que desborda usamos la propiedad ` overflow` que permite establecer estos valores (entre otros menos habituales):
  - `auto`: si el contenido no coge en la caja lo que quede había sido no se verá y aparecerán unas barras de  scroll en la caja para poder ver ese contenido
  - `hidden`: el contido que desborde no se verá.
  - `visible`: el contido que desborde se verá fuera de la caja. Este es el valor por defecto.

#### Límites de ancho e alto

- Cuando definimos una caja con ancho o alto porcentual, por ejemplo 50% podemos definir unos valores máximos de los que no queremos que se pase aunque el tamaño del contenedor crezca. 
- Para esto usamos las propiedades ` max-width,  max-height, min-width y min-height` y la caja tomará eses valores en caso de que el ancho o alto calculado por las propiedades ` width` y ` height` salga de los límites que establecemos.

#### Display

- Todo el comportamiento del modelo de cajas descrito en los anteriores puntos se aplica a los elementos de tipo **block**, pero ya sabemos que hay otros elementos que son de tipo **inline**. Veremos ahora las particularidades de cada tipo con respeto al modelo de cajas y como modificar mediante  CSS el tipo de cada caja. Y veremos que hay otros tipos de cajas que podemos definir usando la propiedad ` display` de  CSS:

  - **Cajas de bloque**: los elementos de tipo bloque se apilan unos sobre otros y tienen por defecto el ancho del contedor del que son hijos. Podemos cambiar el ancho y alto de esas cajas con `width` y `height` este tipo de cajas aplica el modelo de cajas tal como vimos hasta agora.
  - **Cajas inline**: los elementos inline fluyen con el texto, aparecen en la misma línea que el texto que los rodea y que otros elementos inline. Su contenido se distribuirá en diferentes líneas si se pasa de tamaño. **No** podemos establecer ancho y alto de este tipo de caajs pero si márgenes (sólo horizontales), bordes y paddings.
  - **Cajas inline-block**: este tipo se puede definir sólo por CSS y funciona como una mezcla de los anteriores. Podemos establecer `width, height e margin` pero no dividirán el contenido en diferentes líneas en diferentes líneas si se pasan de tamaño.

- Podemos modificar el tipo de caja usando la propiedad `display` de CSS:

  ```css
  h1 {
    display: inline;
  }

  strong {
    display: block;
  }

  span {
    display: inline-block;
  }
  ```

- Hay otros tipos de cajas que se llaman `flex` y `grid` que veremos más adelante.

### Estilo de cajas

#### Fondo

- El fondo de una caja es el área posterior al espacio que ocupa su contenido, padding y border. Pero no el márgen.

- Podemos especificar fondos de color o de imágen usando una serie de propiedades:

  - `background-color`: permite establecer un color de fondo. Por defecto las cajas tienen como cor de fondo `transparent`:

    ```css
    p {
      padding: 2rem;
      background-color: rebeccapurple;
      color: white;
    }
    ```

  - `background-image`: permite establecer una imágen como fondo usando la notación `url(...)`:

    ```css
    h1 {
      background-image: url(/images/fondo.png);
    }
    ```

  - `background-repeat`: por defecto los fondos de imágen se repiten para cubrir todo el espacio de la caja, esta propiedad permite definir como funciona esa repetición:

    - `no-repeat`: la imágen de fondo non se va a repetir, sólo se mostrará una vez.
    - `repeat-x`: la imágen sólo se repetirá horizontalmente.
    - `repeat-y`: la imágen sólo se repetirá verticalmente.
    - `repeat`: este es el valor por defecto y repetirá la imaxe horizontal e verticalmente hasta cubrir todo el espacio de la caja.

  - `background-position`: en el caso de tener un fondo con una imagen que no se repita esta propiedad nos va a permitir posicionar esa imagen. Por defecto soporta dos valores separados por espacio para especificar las coordenadas `x` e `y` de la imagen con respeto al centro de coordenadas que es la esquina superior izquierda. Aparte de  sorportar cualquiera de las unidades conocidas (px,  rem, %), también soporta  posiciones con nombre (y cualquier combinación entre unas y otras):

    - left, center, right: para posicionamiento horizontal
    - top, center, bottom: para posicionamiento vertical

    ```css
    p {
      background-color: rebeccapurple;
      background-image: url(/images/fondo.png);
      background-repeat: no-repeat;
      background-position: 200px center;
    }
    ```

  - `background-size`: permite establecer el tamaño de la imágen especificada como fondo con `background-image`. Los posibles valores son:

    - Dos valores separados por espacio: establece el tamaño de la imágen de fondo, los dos valores pueden ser en las unidades unidades típicas de CSS o `auto`. El valor por defecto de la propiedad es `auto auto`.
    - `contain`: intenta encajar la imágen dentro del fondo sen recortarla ni modificar las proporciones.
    - `cover`: intenta encajar la imágen en el fondo sin modificar las proporciones pero puede recortarla si es necesario.

- Ampliando conocimiento en la MDN:

  - Fondos múltiples: https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_boxes/Backgrounds#Multiple_backgrounds
  - La propiedad `background` permite establecer varias de las propiedades comentadas anteriormente en una sóla propiedad de CSS, puedes aprender más sobre ella aquí: https://developer.mozilla.org/en-US/docs/Web/CSS/background
  - Gradientes: https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_boxes/Backgrounds#Background_image_gradients

#### Bordes

- El borde de una caja está definido por tres propiedades: anchura, estilo y cor. Por defecto las cajas tienen un borde de anchura `0`, negro e sólido (una línea sin espacios).

- Podemos modificar el borde con la propiedad propiedade `border` que permite definir las tres propiedades en una sóla línea o establecerlas por separado usando: `border-width, border-style e border-color`:

  ```css
  h1 {
    border: 2px solid blue;
  }

  /* A seguinte regla é igual que a anterior pero por separado*/
  h1 {
    border-width: 2px;
    border-style: solid;
    border-color: blue;
  }
  ```

  - `border-width`: establece el ancho del borde en cualquier unidad de CSS (excepto %).
  - `border-style`: los posibles valores de estilo son estes https://developer.mozilla.org/en-US/docs/Web/CSS/border-style#Values. Los típicos valores son `solid` (por defecto), `dotted` y `dashed` que establecen líneas sólidas, de puntos y de rayas, respectivamente.
  - `border-color`: establece el color do borde usando cualquier notación de color de CSS.

- Si que queremos hacer bordes redondeados usamos la propiedad `border-radius` que permite establecer el radio de redondeo de los bordes:

  ```css
  p {
    border: 2px solid red;
    borde-radius: 20px;
  }
  ```

  Se queremos establecer un radio de redondeo diferente para cada esquina podemos establecer cuatro valores separados por espacio que establecerían por orden el radio de: equina superior izquierda, superior derecha, inferior derecha e inferior izquierda:

  ```css
  p {
    border: 2px solid red;
    borde-radius: 20px 20px 0 0;
    /*só teria os as esquinas superiores redondeadas*/
  }
  ```

#### Sombras

- Al igual que el texto (`text-shadow`) las cajas pueden tener sombras usando la misma notación:

  ```css
  h1 {
    box-shadow: 2px 2px 5px black;
  }
  ```

- Loss valores son respectivamente: desplazamiento horizontal, desplazamiento vertical, radio de difusión y color de sombra.

#### Flotando elementos

- La propiedad `float` nos permite sacar elementos del flujo normal en el contenedor posicionándolos en parte esquerda ou derecha y haciendo que el resto de los elementos aparezcan rodeándono.
- Históricamente se usó incorrectamente para definir _layouts_ pero ahora con la aparición de Flexbox y Grid ese uso quedó obsoleto.
- El valor por defecto es *none* pero puede ser *left* o *right*.

```css
.left {
  float: left;
  margin-right: 10px;
  height: 200px;
}
```

```html
<section class="container">
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique
    sapien ac erat tincidunt, sit amet dignissim lectus vulputate. Donec id
    iaculis velit. Aliquam vel malesuada erat.
  </p>

  <aside class="left">
    <blockquote>Cita famosa</blockquote>
  </aside>

  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique
    sapien ac erat tincidunt, sit amet dignissim lectus vulputate. Donec id
    iaculis velit. Aliquam vel malesuada erat.
  </p>
</section>
```

- Vemos en el ejemplo que todos los párrafos se distribuyen alrededor del elemento flotado. Si queremos que uno de los elementos no tenga tenga ese comportamiento podemos establecer la propiedad `clear`, que permite os valores *none*, *left*, *right* e *all*.
- Si establecemos un `clear: left` al segundo párrafo esta hará que no se distribuya alrededor  de los elementos flotados a su izquierda.

#### Posicionando elementos

- Los elementos que siguen el flujo normal del documento tienen unha propiedad de CSS que se llama `position` con valor por defecto `static`.
- Vamos a ver otros tipos de posicionamiento: `relative`, `absolute` y `fixed`.

##### Posicionamento relativo

- A los elementos con `position: relative` vamos poder darle una serie de propiedades de CSS: `top, left, bottom y right` que van a modificar su posición por defecto:

```css
.desplazado {
  position: relative;
  top: 20px;
  left: 20px;
}
```

- El espacio original ocupado en el documento por el elemento posicionado relativamente se mantiene aún que el elemento esté en otra posición.

##### Posicionamento absoluto

- Los elementos con `position: absolute` se posicionan con respecto a su contexto de posicionamiento.

- El contexto de posicionamiento es la posición del primer padre del elemento que tenga `position: relative` y si no hay ninguno el contexto será el elemento `html`, o sea, la parte superior izquierda da páxina.

  ```css
  .absoluto {
    position: absoluto;
    right: 20px;
    bottom: 100px;
  }
  ```

- A diferencia de los elementos posicionados relativamente, estes elementos dejan de ocupar el espacio que ocuparían inicialmente.

##### Posicionamento fijo

- La `position: fixed` funciona exactamente igual que la absoluta pero el contexto de posicionamiento siempre va a ser el viewport de la página (tamaño de ventana visible) por lo que aún que hagamos scroll el elemento siempre se va a mantener en la misma posición.

##### Z-index

- En caso de que dos elementos posicionados absolutamente o relativamente se solapen el que va aparecer arriba será el que tenga la propiedade CSS `z-index` maior. Esta propiedad acepta valores numéricos positivos o negativos sin unidad.
- En caso de no tener establecido un `z-index` aparecerá por arriba el último elemento que esté definido en el HTML.