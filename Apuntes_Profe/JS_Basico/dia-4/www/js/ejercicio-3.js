/* #######################
 * ## Bomba con función ##
 * #######################
 */
'use strict';

function getRandomNumber() {
    return Math.ceil(Math.random() * 10);
}

function deactivateBomb(limit, bombPassword) {
    for (let i = limit; i > 0; i--) {
        const userNumber = Number(prompt(`Intento nº ${i}. Indica un nº:`));

        if (userNumber === bombPassword) return true;
    }
    return false;
}

let password = getRandomNumber();

if (deactivateBomb(10, password)) {
    alert('¡Vives un día más!');
} else {
    alert('¡BOOOOOOM!');
}
