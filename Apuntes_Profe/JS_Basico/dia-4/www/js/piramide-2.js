/**
 * ################
 * ## Pirámide 2 ##
 * ################
 *
 * Crea una función que reciba una altura y dibuje una figura
 * como la que sigue:
 *
 *    1
 *    22
 *    333
 *    4444
 *    55555
 *
 */

'use strict';

const height = 5;

for (let lines = 0; lines < height; lines++) {
    let lineContent = '';

    for (let nums = lines + 1; nums > 0; nums--) {
        lineContent += lines + 1;
    }

    console.log(lineContent);
}
