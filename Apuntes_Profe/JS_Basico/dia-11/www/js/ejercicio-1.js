'use strict';

/**
 *  =======================
 *  ··· P E R S O N A S ···
 *  =======================
 */
const persons = [
    {
        name: 'Pedro',
        age: 35,
        code: 'ES',
        infected: true,
        petName: 'Troski',
    },
    {
        name: 'Elisabeth',
        age: 14,
        code: 'UK',
        infected: true,
        petName: 'Firulais',
    },
    {
        name: 'Pablo',
        age: 25,
        code: 'ES',
        infected: false,
        petName: 'Berritxu',
    },
    {
        name: 'Angela',
        age: 18,
        code: 'DE',
        infected: false,
        petName: 'Noodle',
    },
    {
        name: 'Boris',
        age: 50,
        code: 'UK',
        infected: true,
        petName: 'Leon',
    },
    {
        name: 'Donald',
        age: 69,
        code: 'US',
        infected: false,
        petName: 'Pence',
    },
    {
        name: 'Pepito',
        age: 36,
        code: 'ES',
        infected: false,
        petName: 'Carbón',
    },
];

/**
 *  =======================
 *  ··· M A S C O T A S ···
 *  =======================
 */
const pets = [
    {
        petName: 'Troski',
        type: 'perro',
    },
    {
        petName: 'Firulais',
        type: 'perro',
    },
    {
        petName: 'Berritxu',
        type: 'loro',
    },
    {
        petName: 'Noodle',
        type: 'araña',
    },
    {
        petName: 'Leon',
        type: 'gato',
    },
    {
        petName: 'Pence',
        type: 'perro',
    },
    {
        petName: 'Carbón',
        type: 'gato',
    },
];

/**
 *  =======================
 *  ··· A N I M A L E S ···
 *  =======================
 */
const animals = [
    {
        type: 'perro',
        legs: 4,
    },
    {
        type: 'araña',
        legs: 8,
    },
    {
        type: 'gato',
        legs: 4,
    },
    {
        type: 'loro',
        legs: 2,
    },
    {
        type: 'gallina',
        legs: 2,
    },
];

/**
 *  ===================
 *  ··· P A Í S E S ···
 *  ===================
 */
const countries = [
    {
        code: 'CN',
        name: 'China',
        population: 1439,
        infected: 81999,
    },
    {
        code: 'US',
        name: 'Estados Unidos',
        population: 331,
        infected: 112468,
    },
    {
        code: 'DE',
        name: 'Alemania',
        population: 83,
        infected: 56202,
    },
    {
        code: 'ES',
        name: 'España',
        population: 46,
        infected: 72248,
    },
    {
        code: 'UK',
        name: 'Reino Unido',
        population: 67,
        infected: 17301,
    },
];

/**
 *  ###########################
 *  ## E J E R C I C I O   1 ##
 *  ###########################
 *
 *  Número total de infectados del array de personas.
 *
 */

const infected = persons.filter((person) => person.infected).length;

// console.log(infected);

/**
 *  ###########################
 *  ## E J E R C I C I O   2 ##
 *  ###########################
 *
 *  Número total de infectados en el array de países.
 *
 */

const totalInfected = countries.reduce((acc, country) => {
    return (acc += country.infected);
}, 0); // acc = 0;

// console.log(totalInfected);

/**
 *  ###########################
 *  ## E J E R C I C I O   3 ##
 *  ###########################
 *
 *  País con más infectados.
 *
 */

const mostInfectedCountry = countries.reduce((acc, country) => {
    if (acc.infected < country.infected) acc = country;

    return acc;
});

// console.log(mostInfectedCountry);

/**
 *  ###########################
 *  ## E J E R C I C I O   4 ##
 *  ###########################
 *
 *  Array con el nombre de todas las mascotas.
 *
 */

const petsName = pets.map((pet) => pet.petName);

// console.log(petsName);

/**
 *  ###########################
 *  ## E J E R C I C I O   5 ##
 *  ###########################
 *
 *  Array de españoles con perro.
 *
 */

const spanishWithDog = persons.filter((person) => {
    return (
        person.code === 'ES' &&
        pets.find((pet) => {
            return pet.petName === person.petName && pet.type === 'perro';
        })
    );
});

// console.log(spanishWithDog);

/**
 *  ###########################
 *  ## E J E R C I C I O   6 ##
 *  ###########################
 *
 *  Array con las personas. A mayores, este array debe incluír el objeto con los datos de su mascota
 *
 *  {
 *      name: 'Pedro',
 *      age: 35,
 *      country: 'ES',
 *      infected: true,
 *      petName: {
 *          petName: 'Troski',
 *          type: 'perro',
 *      }
 *  }
 *
 */

const fullPersons = persons.map((person) => {
    return {
        ...person,
        petName: pets.find((pet) => pet.petName === person.petName),
    };
});

// console.log(fullPersons);

/**
 *  ###########################
 *  ## E J E R C I C I O   7 ##
 *  ###########################
 *
 *  Número total de patas de las mascotas de las personas.
 *
 */

const numberOfLegs = persons.reduce((acc, person) => {
    pets.find((pet) => {
        pet.petName === person.petName &&
            animals.find((animal) => {
                if (animal.type === pet.type) acc += animal.legs;
            });
    });

    return acc;
}, 0);

// console.log(numberOfLegs);

/**
 *  ###########################
 *  ## E J E R C I C I O   8 ##
 *  ###########################
 *
 *  Array con las personas que tienen animales de 4 patas
 *
 */

// Filtrar a las mascotas de 4 patas.
const fourLeggedPets = pets.filter((pet) => {
    return animals.find((animal) => {
        return animal.type === pet.type && animal.legs === 4;
    });
});

// Filtrar a las personas con mascotas de 4 patas.
const peopleWith4LeggedPets = persons.filter((person) => {
    return fourLeggedPets.find((pet) => {
        return pet.petName === person.petName;
    });
});

/**
 *  ###########################
 *  ## E J E R C I C I O   9 ##
 *  ###########################
 *
 *  Array de países que tienen personas con loros como mascota.
 *
 */

// Filtrar a las personas con loros.
const personsWithParrots = persons.filter((person) => {
    return pets.find((pet) => {
        return pet.petName === person.petName && pet.type === 'loro';
    });
});

// Filtrar a los países con personas con loros.
const countriesWithParrots = countries.filter((country) => {
    return personsWithParrots.find((person) => {
        return person.code === country.code;
    });
});

// console.log(countriesWithParrots);

/**
 *  #############################
 *  ## E J E R C I C I O   1 0 ##
 *  #############################
 *
 *  Número de infectados totales (en el array de países) de los países con mascotas de ocho patas.
 *
 */

// Filtrar a las mascotas de 8 patas.
const eigthLeggedPets = pets.filter((pet) => {
    return animals.find((animal) => {
        return animal.type === pet.type && animal.legs === 8;
    });
});

// Filtrar a las personas con mascotas de 8 patas.
const personsWith8LeggedPets = persons.filter((person) => {
    return eigthLeggedPets.find((pet) => pet.petName === person.petName);
});

// Obtener el total de infectados de los países con mascotas de 8 patas.
const countriesWith8LeggedPets = countries.reduce((acc, country) => {
    personsWith8LeggedPets.find((person) => {
        if (person.code === country.code) acc += country.infected;
    });

    return acc;
}, 0);

console.log(countriesWith8LeggedPets);
