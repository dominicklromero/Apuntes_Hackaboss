/**
 * #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Crea un nuevo array con los amigos en común que tienen dos usuarios de Instagram. El ejercicio
 * debe ser resuelto sin utilizar ningún método excepto push.
 *
 */

'use strict';

const pacosFollowers = ['Manolo', 'Marta', 'Pablo', 'Kevin'];

const luciasFollowers = ['Pablo', 'Marta', 'Ana'];

const commonFollowers = [];

for (let i = 0; i < luciasFollowers.length; i++) {
    for (let j = 0; j < pacosFollowers.length; j++) {
        if (luciasFollowers[i] === pacosFollowers[j]) {
            commonFollowers.push(luciasFollowers[i]);
        }
    }
}

console.log(commonFollowers);
