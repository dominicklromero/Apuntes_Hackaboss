/**
 * #################
 * ## Ejercicio 9 ##
 * #################
 *
 * Dada la función "stringScramble(string1, string2)" toma los dos parámetros y devuelve true
 * si una parte de los caracteres de string1 se pueden reordenar para que coincidan con los
 * de string2, en caso contrario devuelve false.
 *
 * Por ejemplo: si string1 es "rkqodlw" y string2 es "world" la salida debería devolver true.
 *
 */

'use strict';

function stringScramble(string1, string2) {
    // Convierto en un array el primer string.
    const array1 = string1.split('');

    // Contador en el que almaceno las coincidencias.
    let count = 0;

    for (const letter2 of string2) {
        for (let i = 0; i < array1.length; i++) {
            // Si la letra del segundo string coincide con
            // alguna letra del primer string...
            if (letter2 === array1[i]) {
                array1.splice(i, 1); // Eliminamos la letra.
                count++; // Incrementamos el contador.
                break; // Rompemos el bucle interno.
            }
        }
    }

    if (string2.length === count) return true;

    return false;
}

const result = stringScramble('wwchsdoadla', 'hola');

console.log(result);
