/**
 * #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dada la función "letterCapitalize(string)" tome el parámetro string que se le pasa y ponga
 * en mayúscula la primera letra de cada palabra. Las palabras estarán separadas por un solo espacio.
 *
 */

'use strict';

function letterCapitalize(string) {
    const array = string.split(' ');

    for (let i = 0; i < array.length; i++) {
        array[i] = array[i][0].toUpperCase() + array[i].slice(1);
    }

    console.log(array);
}

letterCapitalize('estudiar javascript es una pasada');
