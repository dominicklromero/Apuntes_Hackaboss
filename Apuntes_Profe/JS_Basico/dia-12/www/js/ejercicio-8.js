/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Dado el siguiente tablero (un array de arrays) haz las modificaciones necesarias para
 * lograr esto:
 *
 *     - Figura 1:
 *
 *      [
 *          ['X', '-', '-', '-', 'X'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['-', '-', 'X', '-', '-'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['X', '-', '-', '-', 'X']
 *      ];
 *
 *
 *     - Figura 2:
 *
 *      [
 *          ['X', 'X', 'X', 'X', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', 'X', 'X', 'X', 'X']
 *      ];
 *
 */

'use strict';

const board = [
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
];

/* let counter = board.length - 1;

for (let i = 0; i < board.length; i++) {
    board[i][i] = 'X';
    board[i][counter] = 'X';
    counter--;
} */

for (let i = 0; i < board.length; i++) {
    if (i === 0 || i === board.length - 1) {
        board[i] = ['X', 'X', 'X', 'X', 'X'];
    } else {
        board[i][0] = 'X';
        board[i][board.length - 1] = 'X';
    }
}

console.log(board);
