/**
 * #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Dada la función "alphabetSoup(string)" tome el parámetro string y devuelve la cadena con las
 * letras en orden alfabético (es decir, "hola" se convierte en "ehllo").
 *
 */

'use strict';

function alphabetSoup(string) {
    const array = string.split('');

    const sortedArray = array.sort((a, b) => a.localeCompare(b));

    console.log(sortedArray.join(''));
}

alphabetSoup('HellO');
