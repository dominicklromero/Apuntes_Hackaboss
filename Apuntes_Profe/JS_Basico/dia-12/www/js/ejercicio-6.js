/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 * Dada la función "simpleMode(numsArray)" tome el aprámetro numsArray y devuelva el número
 * que aparece con más frecuencia.
 *
 * Por ejemplo: si numsArray contiene [10, 4, 5, 2, 4] la salida debería ser 4. Si existen
 * dos números que se repiten el mismo número de veces devuelve el que apareció primero en el
 * array (por ejemplo [5, 10, 10, 6, 5] debería devolver 5 porque apareció primero).
 *
 * Si no hay ningún modo, devuelve -1. El array no estará vacío.
 *
 */

'use strict';

function simpleMode(numsArray) {
    // Aquí guardaremos el valor del modo (si lo hay, si no se queda null).
    let mode = null;

    // Este contador irá contando el nº de veces que se repite el valor actual (numsArray[i]);
    let countCurrentNum = 0;

    // Este contador almacena el nº de veces que se ha repetido el modo actual.
    let countMode = 0;

    for (let i = 0; i < numsArray.length; i++) {
        // Por cada nº "numsArray[i]" recorremos todas las posiciones de su derecha.
        for (let j = i + 1; j < numsArray.length; j++) {
            // Si el número que estamos comprobando se repite...
            if (numsArray[i] === numsArray[j]) {
                // Aumentamos en 1 el contador.
                countCurrentNum++;
            }
        }

        // Si el nº actual se repite más veces que el nº que tengamos asignado en el
        // modo lo cambiamos.
        if (countCurrentNum > countMode) {
            mode = numsArray[i];
            // Actualizamos el nº de veces que se repite el modo actual.
            countMode = countCurrentNum;
        }

        // Reiniciamos el contador para que este apunto para el siguiente nº numsArray[i].
        countCurrentNum = 0;
    }

    if (mode === null) {
        console.log(-1);
    } else {
        console.log(mode);
    }
}

simpleMode([10, 4, 5, 2, 4]);
