/**
 * #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Dada la función "timeConvert(minutes)" toma el parámetro minutes y devuelve el número
 * de horas y minutos a los que el parámetro se convierte (es decir, si minutes = 63 entonces
 * la salida debería ser 1:03). Separe el número de horas y minutos con dos puntos.
 *
 */

'use strict';

function timeConvert(minutes) {
    let hour = 0;

    for (minutes; minutes >= 60; minutes -= 60) {
        hour++;
    }

    minutes = minutes < 10 ? '0' + minutes : minutes;

    console.log(`${hour}:${minutes}`);
}

timeConvert(63);
