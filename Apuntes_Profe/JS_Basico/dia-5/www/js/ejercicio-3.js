/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea un programa que simule las tiradas de un dado:
 *
 *      - El dado debe generar en cada tirada un valor entre 1 y 6 (incluídos).
 *
 *      - En cada tirada mostrar el valor del dado y el total acumulado.
 *
 *      - Cuando el total de tiradas supere o iguale los 100 puntos muestra
 *        un mensaje indicando que se han alcanzado los 100 puntos.
 *
 */

'use strict';

// Generar un valor entero aleatorio del 1 al 6.
const dice = () => Math.ceil(Math.random() * 6);

// Contador para almacenar las tiradas del dado.
let total = 0;

// Empezamos a tirar los dados.
for (let i = 1; total < 100; i++) {
    // Valor de la tirada.
    const diceValue = dice();

    // En cada tirada acumulamos los valores que saque el dado.
    total += diceValue;

    console.log(`Tirada ${i}, ha salido un ${diceValue}.`);
    console.log(`Tienes un total de ${total} puntos.`);
    console.log('===================');
}

// Cuando la partida finaliza lo indicamos.
console.log('¡FIN DE LA PARTIDA!');

/**
 * for (let total = 0; total < 100; total += dice()) {
 *   console.log(total);
 * }
 */
