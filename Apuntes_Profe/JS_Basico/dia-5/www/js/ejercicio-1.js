/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea una función "showInfo()" a la que le pases como parámetros un nombre y una edad:
 *
 *      - La función deve devolver un string del tipo: "Eres Pablo y tienes 33 años". Recuerda
 *        que el nombre y la edad son parámetros, pueden variar en cada llamado.
 *
 *      - Muestra lo que devuelva la función por consola. Aplica lo aprendido sobre "template
 *        strings" (template literals).
 *
 *      - Llama a la función tres veces pasándole distintos valores.
 *
 */

const showInfo = (name, age) => `Eres ${name} y tienes ${age} años`;

console.log(showInfo('Pepe', 45));
console.log(showInfo('Lara', 43));
console.log(showInfo('Sofía', 25));
