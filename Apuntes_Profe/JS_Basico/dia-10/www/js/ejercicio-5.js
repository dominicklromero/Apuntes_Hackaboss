/* #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Ordena el siguiente array numérico de menor a mayor: [4, 10, 7, 1, 2]
 *
 * ¡No se puede usar el método sort()!
 *
 */

'use strict';

const nums = [10, 7, 5, 2, 1];

for (let j = 0; j < nums.length - 1; j++) {

    for(let i = 0; i < nums.length; i++) {

        // Comprobamos si la posición actual es mayor que la siguiente...
        if (nums[i] > nums[i + 1]) {
            // Intercambiamos los valores.
            [nums[i], nums[i + 1]] = [nums[i + 1], nums[i]]; 
        }

    }

}

console.log(nums);
