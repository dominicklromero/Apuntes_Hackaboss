/**
 * Accedemos a la clase Array y agregamos un nuevo método de instancia. Este método
 * será heredado por todos los arrays que declaremos posteriormente.
 *
 * Por otro lado, el método map no recibe como parámetro ningún array. ¿Cómo sabe entonces
 * a qué array nos referimos? Un método no es más que una función ligada a un objeto, y
 * cuando se llama a una función se genera un nuevo contexto de ejecución.
 *
 * En ese contexto el objeto que manda sería el array en cuestion, por tanto, podemos
 * utilizar la palabra reservada "this" como sustituto del nombre del array. ¿Cómo si no va
 * a saber este método a qué array nos referimos? ¿Cómo podría adivinar qué nombre le hemos
 * puesto? Eso sería imposible.
 */

'use strict';

const numbers = [2, 4, 13, 24];

Array.prototype.myMap = function (callback) {
    const newArray = [];

    for (let i = 0; i < this.length; i++) {
        const value = callback(this[i], i, this);

        newArray.push(value);
    }

    return newArray;
};

const result = numbers.myMap((num) => num + 5);

console.log(result);
