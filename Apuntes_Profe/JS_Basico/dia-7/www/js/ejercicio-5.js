/* #################
 * ## Ejercicio 5 ##
 * #################
 *
 * - Cuenta el número de letras "r" en el siguiente fragmento de texto:
 *   "Tres tristes tigres tragan trigo en un trigal."
 *
 * - Ahora cuenta las "t". Debes contar las mayúsculas y las minúsculas.
 *
 * - Sustituye todas las "e" por "i".
 *
 */

'use strict';

const myText = 'Tres tristes tigres tragan trigo en un trigal.';

let count_R = 0;
let count_T = 0;

for (const letter of myText) {
    if (letter === 'r') count_R++;
    if (letter.toLowerCase() === 't') count_T++;
}

console.log('Letras r: ', count_R);
console.log('Letras t: ', count_T);

const newText = myText.replaceAll('e', 'i');

console.log(newText);
