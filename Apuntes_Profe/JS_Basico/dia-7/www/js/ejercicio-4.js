/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea un programa que compruebe si un string es palíndromo, es decir, que se lee igual
 * del derecho que del revés. Por ejemplo, "Arriba la birra" es un palíndromo.
 *
 */

'use strict';

let myText = 'Arriba la birra';

function reverseString(string) {
    return string.split('').reverse().join('');
}

// Limpiar la cadena de texto y la transformamos a minúscula.
myText = myText.toLowerCase().replaceAll(' ', '');

// Creo una variable donde almaceno el string invertido.
const reversedText = reverseString(myText);

// Hago la comparación.
console.log(myText === reversedText);
