/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Dada la siguiente API, obtén un array con el nombre de todas las provincias de España.
 *
 * API: https://www.el-tiempo.net/api
 *
 */

'use strict';

fetch('https://www.el-tiempo.net/api/json/v2/provincias')
    .then((response) => response.json())
    .then((data) => {
        const { provincias } = data;

        const nombreProvincias = provincias.map((provincia) => {
            return provincia.NOMBRE_PROVINCIA;
        });

        console.log(nombreProvincias);
    })
    .catch((error) => console.log(error));
