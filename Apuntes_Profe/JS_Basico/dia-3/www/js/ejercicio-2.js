/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * - Crea un bucle "while" que recorra los números del 0 al 100 saltando de 10 en 10.
 *
 * - Posteriormente crea otro bucle "while" que recorra los números del 100 al 0 de
 *   25 en 25.
 */

'use strict';

let counter_A = 0;
let counter_B = 200;

while (counter_A < 101) {
    console.log(counter_A);
    counter_A += 10;
}

console.log('---------');

while (counter_B > -1) {
    console.log(counter_B);
    counter_B -= 25;
}
