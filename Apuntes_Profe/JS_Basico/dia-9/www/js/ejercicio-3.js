/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dado un array de frutas, genera un nuevo objeto en el que cada fruta pase a ser una
 * nueva propiedad del objeto. El valor asignado a esta propiedad debe ser el nº de veces
 * que la fruta se repite en el array.
 *
 *      const fruitBasket = ['naranja', 'naranja', 'limón', 'pera', 'limón', 'plátano', 'naranja'];
 *
 * Para el array anterior, el objeto resultante debería ser:
 *
 *      const fruits = {
 *          naranja: 3,
 *          limón: 2,
 *          pera: 1,
 *          plátano: 1
 *      };
 */

'use strict';

const fruitBasket = [
    'naranja',
    'naranja',
    'limón',
    'pera',
    'limón',
    'plátano',
    'naranja',
];

// 1. Crear el objeto.
const myObject = {};

// 2. Recorrer el array.
for (const fruit of fruitBasket) {
    // 3. Comprobar si la propiedad existe.
    if (fruit in myObject) {
        // 4. Si existe incrementamos la fruta en 1.
        myObject[fruit]++;
    } else {
        // 5. Si no existe creamos la fruta y la inicializamos a 1.
        myObject[fruit] = 1;
    }
}

console.log(myObject);

// console.log(!!fruits.apple);

// console.log('apple' in fruits);

// console.log(fruits['apple'] === undefined);
