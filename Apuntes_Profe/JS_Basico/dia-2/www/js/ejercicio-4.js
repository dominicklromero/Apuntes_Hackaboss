/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea una calculadora con "switch" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: no se puede dividir entre 0.
 */

'use strict';

const num_A = 4;
const num_B = 0;
const option = 3;

switch (option) {
    case 0:
        console.log(num_A + num_B);
        break;

    case 1:
        console.log(num_A - num_B);
        break;

    case 2:
        console.log(num_A * num_B);
        break;

    case 3:
        if (num_B === 0) {
            throw new Error('No se puede dividir entre 0');
        } else {
            console.log(num_A / num_B);
        }
        break;

    case 4:
        console.log(num_A ** num_B);
        break;

    default:
        throw new Error('Tipo de operación incorrecto.');
}
