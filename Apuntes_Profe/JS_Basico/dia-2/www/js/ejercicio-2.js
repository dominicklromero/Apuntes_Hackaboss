/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Dado el valor "num_a = 8" y "num_b = 2":
 *  - Muestra por consola la suma de A + B.
 *  - Muestra el resto de dividir A entre B.
 *  - Muestra el resultado de elevar A a la potencia de B.
 */

'use strict';

const num_A = 8;
const num_B = 2;

console.log(num_A + num_B);
console.log(num_A % num_B);
console.log(num_A ** num_B);
