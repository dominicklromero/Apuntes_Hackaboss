/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea una variable que almacene tu nombre y otra variable que almacene
 * tu número favorito.
 *  - Muestra ambos valores por consola.
 *  - Cambia tu nº favorito por cualquier otro nº.
 *  - Muestra por consola el resultado del cambio.
 *  - Muestra por consola el tipo de las variables definidas.
 */

'use strict';

const nombre = 'Sara';

let numeroFavorito = 14;

console.log(nombre, numeroFavorito);

numeroFavorito = 7;

console.log(numeroFavorito);
console.log(typeof nombre);
console.log(typeof numeroFavorito);
