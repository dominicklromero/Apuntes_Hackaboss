'use strict';

let animal = 1;

/**
 * #############
 * ## Else If ##
 * #############
 */

if (animal === 0) {
    console.log('perro');
} else if (animal === 1) {
    console.log('gato');
} else if (animal === 2) {
    console.log('ratón');
} else {
    console.log('No se reconoce el animal.');
}

/**
 * ############
 * ## Switch ##
 * ############
 */

switch (animal) {
    case 0:
        console.log('perro');
        break;

    case 1:
        console.log('gato');
        break;

    case 2:
        console.log('ratón');
        break;

    default:
        console.error('No se reconoce al animal');
}
