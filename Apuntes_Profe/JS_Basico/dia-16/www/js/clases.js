'use strict';

class Person {
    // Asignamos una propiedad que tendrán todas las personas
    // sí o sí.
    isAdult = false;

    // En el método constructor se definen todas las propiedades
    // que queremos definir en las nuevas instancias.
    constructor(name, age = 10) {
        this.name = name;
        this.age = age;
    }

    // Métodos estáticos
    static test() {
        return `Hello from ${this.name}`;
    }

    // Métodos de instancia
    sayHello() {
        return `Hi! I'm ${this.name}`;
    }
}

// Crear una nueva instancia de Person.
const Manolo = new Person('Manolo');

console.log(Manolo);

// Llamamos a un método estático.
console.log(Person.test());

// Llamamos a un método de instancia.
console.log(Manolo.sayHello());
