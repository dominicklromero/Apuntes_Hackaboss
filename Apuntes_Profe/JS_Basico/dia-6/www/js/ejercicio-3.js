/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dados el siguiente array: [true, false, false, false, true, false, false, false, true]
 *
 *  - Los valores positivos son infectados.
 *
 *  - Los infectados transmiten su enfermedad a los no infectados que tienen a los lados.
 *
 * En este caso el resultado debería ser: [true, true, false, true, true, true, false, true, true];
 *
 * Bonus: las personas infectadas deben curarse una vez liberado el virus.
 *
 */

'use strict';

const infected = [true, true, false, false, true, false, false, false, true];

// Variable que evita que infectemos a un paciente que ha sido curado.
let infectLeft = true;

// Spread operator --> ...
const result = [...infected];

for (let i = 0; i < infected.length; i++) {
    // Si la persona actual (i) está infectada...
    if (infected[i]) {
        // Me aseguro de no meter cosas en la posición -1.
        if (i > 0 && infectLeft) {
            result[i - 1] = true;
        }

        // Me aseguro de no infectar a la persona de mi derecha
        // si me encuentro en la última posición del array.
        if (i < infected.length - 1) {
            result[i + 1] = true;
        }

        result[i] = false;
        infectLeft = false;
    } else {
        infectLeft = true;
    }
}

console.log(result);
