'use strict';

let age = 21;

if (age >= 18) {
    console.log('Mayor de edad');
} else {
    console.log('Menor de edad');
}

/**
 * El operador ternario es exactamente igual que un "if...else".
 *
 * El operador ternario sigue esta estructura:
 *
 *   - condición ? Valor que retorna si se cumple : Valor que retorna si no se cumple
 *
 * Es importante tener en cuenta que el operador ternario tiene un return implícito
 * al igual que las arrow functions.
 *
 * El bloque de código que escribimos a la derecha de la interrogación (?) se ejecuta
 * en caso de la condición se cumpla. De lo contrario (else) se ejecuta el bloque situado
 * a la derecha de los dos puntos (:).
 */

const result = age >= 18 ? 'Mayor de edad' : 'Menor de edad';

// Fijaos como hemos almacenado el string en la variable "result". Esto es gracias al return
// implícito del operador ternario.
console.log(result);

/**
 * En este ejemplo genero un nº entero aleatorio entre 0 y 1. En JavaScript el valor 0 equivale
 * a un valor booleano falso, y el 1 a un valor booleano verdadero.
 *
 *   · 0 es equivalente a false
 *   · 1 es equivalente a true
 *
 * Aprovechándome de esta ventaja y utilizando el operador ternario convierto el valor numérico
 * a una palabra:
 *
 *   · 0 = 'cero'
 *   · 1 = 'uno'
 *
 */
const randomNumber = Math.floor(Math.random() * 2);

console.log(randomNumber);

const numToWord = randomNumber ? 'Uno' : 'Cero';

console.log(numToWord);
