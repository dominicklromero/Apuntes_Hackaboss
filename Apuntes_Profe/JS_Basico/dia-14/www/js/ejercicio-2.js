/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Obtener un array con los todos personajes. Debe existir la posibilidad de filtrar por "status" y
 * por "species".
 *
 * ¡No utilizar el método filter!
 *
 * API: https://rickandmortyapi.com/
 *
 */

'use strict';

async function getCharacters(status = '', species = '') {
    const URL = `https://rickandmortyapi.com/api/character/?status=${status}&species=${species}`;

    try {
        const response = await fetch(URL);
        const data = await response.json();
        const characters = [...data.results];
        let nextPage = data.info.next;

        while (nextPage !== null) {
            const response = await fetch(nextPage);
            const data = await response.json();

            characters.push(...data.results);

            nextPage = data.info.next;
        }

        console.log(characters);
    } catch (error) {
        console.log(error);
    }
}

getCharacters('dead', 'alien');
