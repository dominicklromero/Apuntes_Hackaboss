/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Obtener un array con las series de las 5 primeras páginas
 *
 * API: https://www.episodate.com/api
 *
 */

const getSeries = async (limit) => {
    const series = [];

    for (let i = 1; i <= limit; i++) {
        try {
            const response = await fetch(
                `https://www.episodate.com/api/most-popular?page=${i}`
            );
            const { tv_shows } = await response.json();
            series.push(...tv_shows);
        } catch (error) {
            console.log(error);
        }
    }

    console.log(series);
};

getSeries(5);
