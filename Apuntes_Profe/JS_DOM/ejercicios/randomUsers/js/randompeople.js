/**
 * Cada <li> debería tener una estructura similar a esta:
 *
 * <article>
 *      <header>
 *          <img src="${imagenUsuario}" alt="${nombreCompleto}">
 *          <h1>${nombreCompleto}</h1>
 *      </header>
 *      <p>${ciudad} (${país}), ${añoNacimiento}</p>
 * </article>
 *
 *
 * API: https://randomuser.me/api/?results=10
 */

// Seleccionamos los nodos que vamos a necesitar.
const ul = document.querySelector('ul.userlist');
const liLoading = document.querySelector('li.loading');
const frag = document.createDocumentFragment();

// Función asíncrona que crea un nº X de usuarios.
const getUsers = async (limit) => {
    try {
        const response = await fetch(
            `https://randomuser.me/api/?results=${limit}`
        );

        // Obtenemos el array de usuarios con destructuring.
        const { results } = await response.json();

        // Recorremos el array.
        for (const user of results) {
            // Almacenamos la info que nos interesa.
            console.log(user);
            const picture = user.picture.large;
            const { first, last } = user.name;
            const { city, country } = user.location;
            const birthYear = new Date(user.dob.date).getFullYear(); // new Date('1961-02-10T10:50:09.553Z');

            // Creamos un li.
            const li = document.createElement('li');

            // Insertamos en el li toda la estructura.
            li.innerHTML = `
                <article>
                    <header>
                        <img src="${picture}" alt="${first} ${last}">
                        <h1>${first} ${last}</h1>
                    </header>
                    <p>${city} (${country}), ${birthYear}</p>
                </article>
            `;

            // Insertamos el li como hijo del fragmento.
            frag.append(li);
        }

        // Eliminamos el li loading.
        liLoading.remove();

        // Insertamos el contenido del fragmento como hijo delul.
        ul.append(frag);
    } catch (error) {
        console.log(error);
    }
};

// Utilizamos un setTimeout para que de tiempo a mostrarse al li de loading...
setTimeout(() => {
    const numberOfUser = Number(
        prompt('Inserte un nº de usuarios del 1 al 100:')
    );
    getUsers(numberOfUser);
}, 500);
