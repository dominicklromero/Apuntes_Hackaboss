/**
 * Ejemplo de la estructura final:
 *
 *  <ul id="tweets">
 *      <li>
 *          <p>Lo que escriba el usuario en el input.</p>
 *          <footer>
 *              <time>23/3/2021</time>
 *              <button class="action">Borrar</button>
 *          </footer>
 *      </li>
 *      <li>
 *          <p>Lo que escriba el usuario en el input.</p>
 *          <footer>
 *              <time>23/3/2021</time>
 *              <button class="action">Borrar</button>
 *          </footer>
 *      </li>
 *      <li>
 *          (...)
 *      </li>
 *  </ul>
 */
