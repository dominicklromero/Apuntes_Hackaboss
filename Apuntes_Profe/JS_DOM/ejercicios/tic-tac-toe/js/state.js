'use strict';

// Obtenemos todos los elementos necesarios desde el localStorage.
const storageBoard = '';
const storageRound = '';

/**
 *  Inicializamosel estado. Si hay algún valor en el localStorage lo tomamos de ahí,
 *  de lo contrario inicializamos por defecto:
 *
 *      - board = Array(9).fill(null)
 *      - round = 0
 *
 */
const State = {
    board: '',
    round: '',
};

/**
 * ###############
 * ## saveState ##
 * ###############
 *
 * Función que se encargade guardar en localStorage el estado actual del State.
 * Recuerda que el State tiene dos propiedades, debes guardar las dos.
 *
 */
const saveBoard = () => {};

/**
 * #################
 * ## updateBoard ##
 * #################
 *
 * En funcion de la casilla que seleccione el jugador actual cambiamos el valor
 * del tablero y pasamos a la siguiente ronda.
 *
 * Guardamos el State.
 *
 */

const updateBoard = (index, value) => {};

/**
 * ################
 * ## resetBoard ##
 * ################
 *
 * Si el juego finaliza debemos permitir al jugador resetear la partida a los valores
 * por defecto.
 *
 * Guardamos el State.
 *
 */

const resetBoard = () => {};

/**
 * #################
 * ## checkWinner ##
 * #################
 *
 * Comprobamos si ya hay un ganador. Para ello debemos comprobar todas las combinaciones ganadoras
 * posibles.
 *
 *  - Si el ganador es el jugador 1 (la X) retornamos un mensaje de victoria.
 *
 *  - Si el ganador es el jugador 2 (la O) retornamos un mensaje de victoria.
 *
 *  - Si no hay más intentos y hubo empate retornamos un mensaje de empate.
 *
 *  - En cualquier otra circunstancia retornamos un false;
 *
 */
const checkWinner = () => {
    // Combinaciones posibles para una victoria.
    const winningLines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    // Recorremos las combinaciones posibles.
    for (let i = 0; i < winningLines.length; i++) {
        // Desestructuramos los 3 valores de  la linea actual.
        const [a, b, c] = winningLines[i];
        // Si esto se cumple...
        if (
            State.board[a] &&
            State.board[a] === State.board[b] &&
            State.board[a] === State.board[c]
        ) {
            // ...quiere decir que alguno de los dos jugadores ha ganado.
        }
    }
};

export default State;
export { updateBoard, resetBoard, checkWinner };
