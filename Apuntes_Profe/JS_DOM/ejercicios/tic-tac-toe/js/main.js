'use strict';

import State, { updateBoard, resetBoard, checkWinner } from './state.js';

// Seleccionamos el main.
const main;

// Seleccionamos el tablero: div con clase "board".
const boardDiv;

// Seleccionamos las tres lineas de cuadrados: los tres divs dentro del anterior.
const firstRowDiv;
const secondRowDiv;
const thirdRowDiv;

/**
 * #######################
 * ## handleSquareClick ##
 * #######################
 *
 * Esta función manejadora dicta lo que sucede cada vez que un jugador
 * hace click en un cuadrado.
 *
 *  - Si el objetivo es el div con clase "square" comprobamos qué jugador
 *    está jugando: rondas pares "X" rondas impares "O".
 *
 *  - Posteriormente obtenemos el index del cuadrado sobre el que pulsamos.
 *
 *  - Actualizamos el tablero.
 *
 *  - Renderizamos los cambios en el HTML.
 *
 */
const handleSquareClick = (e) => {};

// Agregamos el evento de click al div con clase "board".


/**
 * ######################
 * ## Resetear partida ##
 * ######################
 * 
 * Agregamos un manejador de evento al main que compruebe si el elemento 
 * clickado es el div con clase "reset". Si es así:
 * 
 *  - Eliminamos el elemento padre.
 * 
 *  - Reseteamos el tablero.
 * 
 *  - Activamos de nuevo la función manejadora "handleSquareClick"
 * 
 */

main.addEventListener('click', (e) => {

});

/**
 * ############
 * ## Render ##
 * ############
 */

function render() {
    
    // Vaciamos las tres filas de casillas.

    // Creamos las filas. Para ello debemos recorrer el tablero (board).
    
    // Creamos un div y le agregamos el contenido de la posición actual 
    // del tablero. 
    
    // Agregamos al div la clase "square" y el atributo "data-index"con
    //  el valor del index actual.

    // Recuerda que el tablero tiene 9 elementos. Los tres primeros son
    // las casillas de la primera fila, los 3 siguientes las casillas de
    // la segunda fila, y los tres últimos son las casillas de la última
    // fila. Agrega las casillas como hijo de la fila correspondiente.

    // Almacenamos el valor que retorne la función winner.

    // Si hay un ganador...
  
    // Eliminamos el event listener que permite clickar en los divs, 
    // es decir, en cada casilla.
    
    // Creamos un div y le agregamos la clase "reset".
        
    // Agregamos al div un párrafo con un mensaje que indique el ganador
    // y un h2 que diga "Try againg".
    
    // Agregamos al main el div.
}

render();
