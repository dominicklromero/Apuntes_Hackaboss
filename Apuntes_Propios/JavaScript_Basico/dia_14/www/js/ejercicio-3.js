/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Obtener un array con los todos personajes. Debe existir la posibilidad de filtrar por "status" y
 * por "species".
 *
 * ¡No utilizar el método filter!
 *
 * API: https://rickandmortyapi.com/
 *
 */

'use strict';

const arrayVacio = [];

const getCharacter = async (status, species) => {
    let URL;

    if (!status && !species) {
        URL = 'https://rickandmortyapi.com/api/character/';
    } else if (!status) {
        URL = `https://rickandmortyapi.com/api/character/?species=${species}`;
    } else if (!species) {
        URL = `https://rickandmortyapi.com/api/character/?status=${status}`;
    } else {
        URL = `https://rickandmortyapi.com/api/character/?status=${status}&species=${species}`;
    }

    try {
        const call1 = await fetch(URL);

        const call2 = await call1.json();

        const paginas = call2.info.pages;

        for (let i = 1; i <= paginas; i++) {
            const res = await fetch(`${URL}&page=${i}`);

            const character = await res.json();

            arrayVacio.push(...character.results);
        }
    } catch (error) {
        console.log(error);
    }
    console.log(
        arrayVacio.map((character) => {
            return character.name;
        })
    );
};

/* const status = prompt('Dime un el status:');
const species = prompt('Dime un la especie:');
getCharacter(status, species); */

getCharacter('Alive', 'Human');
