/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Obtener un array con los todos personajes humanos que esten muertos.
 *
 * ¡No utilizar el método filter!
 *
 * API: https://rickandmortyapi.com/
 *
 */

'use strict';

const arrVacio = [];

const deadHumanCharacters = async () => {
    try {
        const res1 = await fetch(
            'https://rickandmortyapi.com/api/character/?status=Dead&species=Human'
        );

        const character1 = await res1.json();

        const pages = character1.info.pages;

        for (let i = 1; i <= pages; i++) {
            const res2 = await fetch(
                `https://rickandmortyapi.com/api/character/?page=${i}&status=Dead&species=Human`
            );

            const character2 = await res2.json();

            arrVacio.push(...character2.results);
        }
    } catch (error) {
        console.error(error);
    }

    console.log(
        arrVacio.map((nombre) => {
            return nombre.name;
        })
    );
};

deadHumanCharacters();
