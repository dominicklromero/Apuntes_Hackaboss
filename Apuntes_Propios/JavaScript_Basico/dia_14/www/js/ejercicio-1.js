/**
 * ###############################################################
 * ## Obtener un array con las series de las 5 primeras páginas ##
 * ###############################################################
 *
 * API: https://www.episodate.com/api
 *
 */

'use strict';

const array5paginas = [];

const getSeries = async (page) => {
    try {
        const res = await fetch(
            `https://www.episodate.com/api/most-popular?page=${page}`
        );

        const series = await res.json();

        series.tv_shows.map((serie) => {
            return array5paginas.push(serie.name); // const {tv_shows} = await res.json()
        }); // array5paginas.push(...tv_shows)
    } catch (error) {
        console.log(error);
    }
};

for (let i = 1; i <= 5; i++) {
    getSeries(i);
}

console.log(array5paginas);
