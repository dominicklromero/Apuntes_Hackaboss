/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea una calculadora con "if" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: no se puede dividir entre 0.
 */

'use strict';

let numA = parseInt(prompt('Dime un numero:'));
let numB = parseInt(prompt('Dime otro numero:'));

let option = prompt('Dime la operacion');

if (option === 'suma') {
    console.log(numA + numB);
} else if (option === 'resta') {
    console.log(numA - numB);
} else if (option === 'multiplicacion') {
    console.log(numA * numB);
} else if (option === 'division' && numA !== 0 && numB !== 0) {
    console.log(numA / numB);
} else if (option === 'potencia') {
    console.log(numA ** numB);
} else {
    console.log('Error');
}
