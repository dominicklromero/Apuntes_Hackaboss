console.log('Hello World');

let edad = 25;
const nombre = 'Alejandro';
let indefinido;
const nulo = null;
console.log(edad);
console.log(nombre);
console.log(typeof edad);
console.log(typeof nombre);
console.log(typeof indefinido);
console.log(typeof nulo);

let animal = 56;

if (animal >= 18) {
    console.log('Perro');
} else if (animal === 0) {
    console.log('Gato');
} else if (animal === 1) {
    console.log('Raton');
} else {
    console.log('No se reconoce el animal');
}

switch (animal) {
    case 0:
        console.log('Perro');
        break;
    case 1:
        console.log('Gato');
        break;
    case 2:
        console.log('Raton');
        break;
    default:
        console.log('No se reconoce al animal');
}
