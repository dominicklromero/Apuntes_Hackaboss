/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea una calculadora con "switch" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: no se puede dividir entre 0.
 */

'use strict';

let num3 = parseInt(prompt('Dime un numero:'));
let num4 = parseInt(prompt('Dime otro numero:'));

let option2 = prompt('Dime la operacion:');

switch (option2) {
    case 'suma':
        console.log(num3 + num4);
        break;
    case 'resta':
        console.log(num3 - num4);
        break;
    case 'multiplicacion':
        console.log(num3 * num4);
        break;
    case 'division':
        if (num3 !== 0 && num4 !== 0) {
            console.log(num3 / num4);
        }
        break;
    case 'potencia':
        console.log(num3 ** num4);
        break;
    default:
        console.log('Error');
}
