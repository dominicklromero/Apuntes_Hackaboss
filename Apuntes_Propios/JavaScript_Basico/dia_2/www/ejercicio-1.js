/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea una variable que almacene tu nombre y otra variable que almacene
 * tu número favorito.
 *  - Muestra ambos valores por consola.
 *  - Cambia tu nº favorito por cualquier otro nº.
 *  - Muestra por consola el resultado del cambio.
 *  - Muestra por consola el tipo de las variables definidas.
 */

'use strict';

let numFav = 9;
const nombre2 = 'Alejandro';

//Apartado_1
console.log('Mi nombre es ' + nombre2 + ' y mi numero favorito es ' + numFav);

//Apartado_2_y_3
numFav = 19;
console.log('Mi numero favorito es ' + numFav);

//Apartado_4
console.log('La variable numFav es una variable de tipo ' + typeof numFav);
console.log('La variable nombre2 es una variable de tipo ' + typeof nombre2);
