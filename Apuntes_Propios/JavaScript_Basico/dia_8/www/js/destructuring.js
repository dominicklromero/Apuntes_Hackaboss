'use strict';

/**
 * ############
 * ## Arrays ##
 * ############
 */

const nums = [12, 140, 35];

// let a = nums[0];
// let b = nums[1];
// let c = nums[2];

const [a, , c] = nums;

console.log(a, c);

/**
 * ############################
 * ## Intercambio de valores ##
 * ############################
 */

let x = 100;
let y = 1;

// const tmp = x; // tmp = 100;
// x = y; // x = 1;
// y = tmp; // y = 100;

// Si te fijas detenidamente no deja de ser destructuring con arrays.
// Se aplica la misma lógica.
[x, y] = [y, x];

console.log(x, y);

/**
 * #############
 * ## Objetos ##
 * #############
 */

const car = {
    brand: 'Opel',
    model: 'Corsa',
};

// let z = car.brand;
// let w = car.model;

let { model, brand: marca } = car;

console.log(model, marca);

// Parámetro de tipo objeto con valores por defecto en una función.
function getInfo({ name = 'David', age = 34 } = {}) {
    return `Me llamo ${name} y tengo ${age} años.`;
}

// Si llamo a la función y no le paso un objeto con un nombre y una edad...
console.log(getInfo());

const sara = {
    name: 'Sara',
    age: 25,
};

// Si le paso a la función el objeto "sara" creado anteriormente...
console.log(getInfo(sara));

// También es posible crear el objeto directamente dentro de los paréntisis
// del llamado a la función.
console.log(getInfo({ name: 'Raquel', age: 48 }));
