/*

##########################
###### Ejercicio 1 #######
##########################

- Crea el objeto coche y asígnale las propiedades modelo, marca y color.

- Muestra el objeto por medio de un "console.log".

- Modifica el valor de la propiedad color y agrega la propiedad año 
  de matriculación.

- Utiliza un "confirm" para mostrar por consola las propiedades, 
  o los valores. Si la persona acepta el "confirm" se mostrarán las propiedades, 
  de lo contrario, se mostrarán los valores

*/


'use strict';

const Coche = {
    modelo: 'R8',
    marca: 'Audi',
    color: 'Azul'
};

Coche.color = 'Rojo';
Coche.añoMatriculacion = '2016';

console.table(Coche); //Te muestra el objeto en una tabla.

const whatShow = confirm('Que quieres mostrar? Propiedades(Acepta) || Valores(Cancela)');

if (whatShow) {
    console.log(Object.keys(Coche)); //Con esto mostramos las propiedades de un objeto.
} else {
    console.log(Object.values(Coche)); //Con esto mostramos los valores de un objeto.
}

/* delete Coche.color borra una propiedad */