'use strict';

/**
 * N U M B E R
 */

const num = Number('123r');

// Si el método "Number.isNaN()" devuelve true NO es un número.
if (Number.isNaN(num)) {
    console.error('No es un número.');
} else {
    console.log('Es un número.');
}

/**
 * S T R I N G
 */

const myText = 'Hola ola caracola';

if (typeof myText !== 'string') {
    console.error('No es un string.');
} else {
    console.log('Es un string.');
}

/**
 * A R R A Y
 */

const myArray = ['Hola'];

if (Array.isArray(myArray)) {
    console.log('Es un array');
} else {
    console.error('No es un array');
}

/**
 * O B J E C T
 */

const car = {
    brand: 'Opel',
    model: 'Corsa',
};

if (car.constructor.name !== 'Object') {
    console.error('No es un objeto');
} else {
    console.log('Es un objeto');
}
