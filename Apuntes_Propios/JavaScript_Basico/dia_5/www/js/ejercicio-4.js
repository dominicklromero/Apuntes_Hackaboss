/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Escribe un programa que permita al usuario introducir elementos en un array.
 * El programa finalizará cuando el usuario introduzca el string "fin", y se
 * mostrará por consola el contenido del array.
 *
 */

const miArray = [];

let acabar = '';

while (acabar !== 'fin') {
    acabar = prompt('Dime una palabra:');
    miArray.push(acabar);
}
console.log(miArray);
