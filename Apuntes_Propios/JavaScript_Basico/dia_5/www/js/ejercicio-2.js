/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Tenemos 3 equipos de baloncesto. Estos son los resultados de cada equipo
 * en los últimos 4 partidos:
 *
 *      - Equipo A: 35, 46, 29, 58
 *      - Equipo B: 46, 72, 26, 36
 *      - Equipo C: 38, 62, 47, 44
 *
 * Muestra por consola el equipo con la mejor media en estos cuatro partidos.
 * Debes mostrar el nombre del equipo y la media de dicho equipo.
 *
 */

'use strict';

const team_A = 35 + 46 + 29 + 58;

const team_B = 46 + 72 + 26 + 36;

const team_C = 38 + 62 + 47 + 44;

const getAverage = (totalPoints, totalGames) => totalPoints / totalGames;

const average_A = getAverage(team_A, 4);

const average_B = getAverage(team_B, 4);

const average_C = getAverage(team_C, 4);

if (average_A > average_B && average_A > average_C) {
    console.log(`El equipo A tiene mejor media (${average_A} puntos)`);
} else if (average_B > average_C) {
    console.log(`El equipo B tiene mejor media (${average_B} puntos)`);
} else {
    console.log(`El equipo C tiene mejor media (${average_C} puntos)`);
}
