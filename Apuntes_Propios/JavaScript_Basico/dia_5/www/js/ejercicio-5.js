/**
 * #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Crea una función que imprima X resultados aleatorios de una
 * quiniela 1 X 2. Ejemplo, si le decimos que imprima 14 resultados:
 *
 *      Resultado 1: 1
 *      Resultado 2: X
 *      Resultado 3: 2
 *      (...)
 *      Resultado 14: 2
 *
 */
'use strict';

function numRandom() {
    const result = Math.floor(Math.random() * 3);
    if (result === 0) {
        return 'X';
    }
    return result;
}

function Resultado(numResult) {
    for (let i = 0; i < numResult; i++) {
        let txResult = `Resultado nº ${i + 1}: ${numRandom()}`;
        console.log(txResult);
    }
}

Resultado(5);
