/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea un programa que simule las tiradas de un dado:
 *
 *      - El dado debe generar en cada tirada un valor entre 1 y 6 (incluídos).
 *
 *      - Cuando el total de tiradas supere o iguale los 100 puntos muestra
 *        un mensaje indicando que se han alcanzado los 100 puntos.
 *
 */

'use strict';

function tiradaDado() {
    const numTirada = Math.ceil(Math.random() * 6);
    return numTirada;
}

let contador = 0;

while (contador < 100) {
    contador += tiradaDado();
    console.log(contador);
}

console.log('Has sobrepasado los 100 pts');
