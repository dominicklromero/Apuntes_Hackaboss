/* #################
 * ## Ejercicio 6 ##
 * #################
 *
 * Dado el array = [1, 3, 9, 14, 17, 22]
 *
 *  - Iterar por todos los elementos dentro de un array utilizando "while" y mostrarlos en pantalla.
 *
 *  - Iterar por todos los elementos dentro de un array utilizando "for" y mostrarlos en pantalla.
 *
 *  - Iterar por todos los elementos dentro de un array utilizando "for of" y mostrarlos en pantalla.
 *
 *  - Mostrar todos los elementos dentro de un array sumándole uno a cada uno.
 *
 *  - Generar otro array con todos los elementos del primer array incrementados en 1. ¿Con el método push?
 *
 *  - Calcular el promedio.
 *
 */

'use strict';

const nums = [1, 3, 9, 14, 17, 22];

let contador = 0;

while (contador < nums.length) {
    console.log(nums[contador]);
    contador++;
}

console.log('###########');

for (let i = 0; i < nums.length; i++) {
    console.log(nums[i]);
}

console.log('###########');

for (const numero of nums) {
    console.log(numero);
}

console.log('###########');

for (const numero of nums) {
    console.log(numero + 1);
}

console.log('###########');

const nums2 = [];

for (let j = 0; j < nums.length; j++) {
    nums2.push(nums[j] + 1);
}

console.log(nums2);

console.log('###########');

let total = 0;

for (let k = 0; k < nums.length; k++) {
    total += nums[k];
}
let media = total / nums.length;
console.log(media);
