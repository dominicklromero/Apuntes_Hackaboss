/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Obtén todos los personajes cuya estatura sea igual o inferior a 160
 *
 * Api URL: https://www.swapi.tech/api
 *
 */

const result1 = [];
const resultTotal = [];

const estaturaChr = async function (categoria) {
    try {

        let Url = 'https://www.swapi.tech/api';

        const res = await fetch(`${Url}/${categoria}`);
        const character = await res.json();

        let totalPages = character.total_pages;

        for (let i = 1; i <= totalPages; i++) {
            const res2 = await fetch(`${Url}/${categoria}/?page=${i}&limit=10`);
            const character2 = await res2.json();
            result1.push(...character2.results);
        }

        for (let j = 0; j < result1.length; j++) {
        const res3 = await fetch(result1[j].url);
        const character3 = await res3.json();

        resultTotal.push(character3.result.properties);

        }

        const altura = resultTotal.filter((propiedad) => {
            return propiedad.height <= 160;
        });

        const nameST = altura.map((nombre) => nombre.name);

        console.log(nameST);

    }

    catch (error) {
        console.log(error);
    }
}

estaturaChr('people');





/* async function queryApi(page) {
    try {
        return await (await fetch (page)).json();
    } catch(error) {
        throw error;
    }
} */
/**
 * ##############################
 * ## getBasicCharactersInfo() ##
 * ##############################
 * 
 * Devuelve un array con información básica de todos los personajes.
 * 
 */
/* async function getBasicCharactersInfo() {
    try {
        const content_1 = await queryApi('https://www.swapi.tech/api/people');
        let nextPage = content_1.next;
        const basicInfo = [...content_1.results];
        while (nextPage !== null) {
            try {
                const content_2 = await queryApi(nextPage);
                // Cambiamos la URL de la página siguiente. Cuando este valor sea null
                // el bucle while finalizará.
                nextPage = content_2.next;
                basicInfo.push(...content_2.results);   
            } catch(error) {
                throw error;
            }
        }
        return basicInfo;
    } catch(error) {
        throw error;
    }
} */
/**
 * #################################
 * ## getCompleteCharactersInfo() ##
 * #################################
 * 
 * Devuelve un array con información detallada de todos los personajes.
 * 
 * Aprovecho esa opción para hacer un filtrado (sin usar el método filter)
 * de los personajes cuya altura sea menor o igual a 160.
 * 
 */
/* async function getCompleteCharactersInfo() {
    try {
        const basicInfo = await getBasicCharactersInfo();
        let completeCharacters = [];
        for (const character of basicInfo) {
            try {
                const content = await queryApi(character.url);
                if (parseInt(content.result.properties.height) <= 160) {
                    // En este caso no usamos el spread operator porque
                    // "properties" no es un array de objetos, es un objeto.
                    completeCharacters.push(content.result);
                } 
            } catch(error) {
                throw error;
            }
        }
        return completeCharacters;
    } catch(error) {
        throw error;
    }
}
getCompleteCharactersInfo().then(console.log); */