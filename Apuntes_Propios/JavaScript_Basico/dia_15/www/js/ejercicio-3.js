/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dados los siguientes arrays: [0, 1, 0, 0, 2, 2] y [2, 0, 0, 2, 1, 0, 2]:
 *
 *  - El valor 1 representa la posición del jugador. Solo hay un jugador.
 *
 *  - El valor 2 representa al enemigo.
 *
 *  - El programa debe indicar a qué distancia está el enemigo más cercano.
 *
 *  - [0, 1, 0, 0, 2, 0] // Output: 3
 *  - [2, 0, 0, 0, 2, 1, 0, 2] // Output: 1
 *
 */

'use strict';

const array1 = [0, 1, 0, 0, 2, 2];
const array2 = [2, 0, 0, 0, 1, 0, 2];

function distancia(array) {

    const jugadorPos = array.indexOf(1);
    const enemigoPosIzq = array.lastIndexOf(2,jugadorPos);
    const enemigoPosDer = array.indexOf(2,jugadorPos);
    const distDer = enemigoPosDer - jugadorPos;
    const distIzq = jugadorPos - enemigoPosIzq;

    if (distDer < distIzq) {
        return distDer;
    } else {
        return distIzq;
    }
}

console.log(distancia(array1));
