/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Utiliza los métodos map, filter o reduce para resolver las siguientes propuestas:
 *
 *  - 1. Obtén la suma total de todas las edades de las personas.
 *  - 2. Obtén la suma total de todas las edades de las personas francesas.
 *  - 3. Obtén un array con el nombre de todas las mascotas.
 *  - 4. Obtén un array con las personas que tengan gato.
 *  - 5. Obtén un array con los coches de los españoles.
 *  - 6. Obtén un array con las personas que tengan un coche de la marca Ford.
 *  - 7. ¡Bonus point! Obtén un array con todas las personas en el que cada persona tenga toda
 *       la info de su coche. Ejemplo a continuación:
 *
 *      [
 *          {
 *               name: 'Berto',
 *               country: 'ES',
 *               age: 44,
 *               car: {
 *                  id: 'LU9286V',
 *                  brand: 'Citroen',
 *                  model: 'Xsara'
 *               },
 *               pet: {
 *                   name: 'Moon',
 *                   type: 'perro'
 *               }
 *           },
 *           (...)
 *      ]
 *
 *  Tip: en algún caso es probable que el método "nombreArray.find()" te sea de ayuda.
 *
 */

'use strict';

const persons = [
    {
        name: 'Berto',
        country: 'ES',
        age: 44,
        car: 'LU9286V',
        pet: {
            name: 'Moon',
            type: 'perro',
        },
    },
    {
        name: 'Jess',
        country: 'UK',
        age: 29,
        car: 'GB2913U',
        pet: {
            name: 'Kit',
            type: 'gato',
        },
    },
    {
        name: 'Tom',
        country: 'UK',
        age: 36,
        car: 'GB8722N',
        pet: {
            name: 'Rex',
            type: 'perro',
        },
    },
    {
        name: 'Alexandre',
        country: 'FR',
        age: 19,
        car: 'FT5386P',
        pet: {
            name: 'Aron',
            type: 'gato',
        },
    },
    {
        name: 'Rebeca',
        country: 'ES',
        age: 32,
        car: 'MD4578T',
        pet: {
            name: 'Carbón',
            type: 'gato',
        },
    },
    {
        name: 'Stefano',
        country: 'IT',
        age: 52,
        car: 'LP6572I',
        pet: {
            name: 'Bimbo',
            type: 'perro',
        },
    },
    {
        name: 'Colette',
        country: 'FR',
        age: 22,
        car: 'FU8929P',
        pet: {
            name: 'Amadeu',
            type: 'gato',
        },
    },
];

const cars = [
    {
        id: 'LU9286V',
        brand: 'Citroen',
        model: 'Xsara',
    },
    {
        id: 'GB2913U',
        brand: 'Fiat',
        model: 'Punto',
    },
    {
        id: 'GB8722N',
        brand: 'Opel',
        model: 'Astra',
    },
    {
        id: 'FT5386P',
        brand: 'Ford',
        model: 'Focus',
    },
    {
        id: 'MD4578T',
        brand: 'Opel',
        model: 'Corsa',
    },
    {
        id: 'LP6572I',
        brand: 'Ford',
        model: 'Fiesta',
    },
    {
        id: 'FU8929P',
        brand: 'Fiat',
        model: 'Uno',
    },
];

// Apartado_1

const edadesPers = persons.reduce((acc,value) => {
    acc += value.age;
    return acc;
},0)

console.log(`La suma total de las edades de las personas es: ${edadesPers}`);


//Apartado_2

const edadesPersFR = persons.filter((person) => {
    return person.country === 'FR';
}).reduce((acc,value) => {
    acc += value.age;
    return acc;
},0)

console.log(edadesPersFR);


//Apartado_3

const mascotas = persons.map((pet) => {
    return pet.pet.name;
})

console.log(mascotas);


//Apartado_4

const persGatos = persons.filter((person) => {
    return person.pet.type === 'gato';
}).map((value) => {
    return value.name;
})

console.log(persGatos);


//Apartado_5

const cochesESP = cars.filter((car) => {
    return persons.find((person) => {
        return person.car === car.id && person.country === 'ES'
    })
}).map((name) => {
    return name.brand;
})

console.log(cochesESP);


//Apartado_6

const fordPers = persons.filter((person) => {
    return cars.find((car) => {
        return person.car === car.id && car.brand === 'Ford'
    })
}).map((name) => {
    return name.name;
})

console.log(fordPers);


// Apartado_7

const unObjeto = persons.map((person) => {
    return {
        name: person.name,
        country: person.name,
        age: person.name,
        car: cars.find((car) => {
            return car.id === person.car;
        }),
        pet: {...person.pet},
    }
})
console.log(unObjeto);