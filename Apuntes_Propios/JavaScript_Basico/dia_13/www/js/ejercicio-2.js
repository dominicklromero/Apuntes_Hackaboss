/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Dada la siguiente API, muéstrame todas las provincias de ESPAÑA!!.
 *
 * API: https://www.el-tiempo.net/api
 *
 */

'use strict';

fetch('https://www.el-tiempo.net/api/json/v2/provincias')
    .then((response) => response.json())
    .then((provincia) => console.log(provincia.provincias.map((prov) => {
        return prov.NOMBRE_PROVINCIA;
    })))
    .catch((error) => console.error(error));

/* console.log(variableConFetchGuardado);  No sirve estamos llamando a contenido asincrono con una sentencia sincrona */
