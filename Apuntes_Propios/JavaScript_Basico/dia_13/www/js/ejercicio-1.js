/* ############
 * ## Alarma ##
 * ############
 *
 * Simula un despertador que suene cuando pasan "x" segundos.
 *
 * - Muestra los segundos por consola.
 *
 * - Cuando la cuenta llegue a 0, muestra un mensaje indicando que suena la alarma.
 *
 */

'use strict';

function alarma(seg) {
    const interval = setInterval(() => {
        if (seg > 0) {
            console.log(`Quedan: ${seg}`);
            seg--;
        } else {
            console.log('RIIIIIIIING');
            clearTimeout(interval);
        }
    }, 1000);
}

alarma(10);
