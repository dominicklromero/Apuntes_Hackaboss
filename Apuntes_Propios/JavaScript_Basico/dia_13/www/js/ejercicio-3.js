/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dada la siguiente API, obtén un array con el nombre de todos los municipios de la
 * provincia de Lugo.
 *
 * Además, el nombre de los municipios debe estar ordenado por orden alfabético inverso.
 *
 * - Resuélvelo con then & catch.
 *
 * - Resuélvelo con async / await.
 *
 * API: https://www.el-tiempo.net/api
 *
 */

'use strict';

fetch('https://www.el-tiempo.net/api/json/v2/provincias/27/municipios') // 27 es el codigo de la provincia de Lugo
    .then((response) => response.json())
    .then((municipio) => {
        console.log(
            municipio.municipios
                .map((muni) => {
                    return muni.NOMBRE;
                })
                .sort()
                .reverse()
        );
    })
    .catch((error) => {
        console.error(error);
    });

async function municLugo() {
    const response = await fetch(
        'https://www.el-tiempo.net/api/json/v2/provincias/27/municipios'
    );

    const data = await response.json();

    return data.municipios.map((muni2) => {
        //Para hacerlo con return debemos partir de que esta funcion devuelve una funcion asincrona (devuelve una promesa)
        return muni2.NOMBRE;
    });
}

municLugo()
    .then((data) => console.log(data.sort().reverse())) // Se pone un then despues del llamado de la funcion asincrona cuando finaliza en return
    .catch((error) => console.error(error));
