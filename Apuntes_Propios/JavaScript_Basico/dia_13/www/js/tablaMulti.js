'use strict';

const nums = [1, 2, 3, 4, 5, 6, 7, 8, 9];

for (let i = 0; i < nums.length; i++) {
    for (let j = 0; j < nums.length; j++) {
        let result = nums[i] * nums[j];
        console.log(`${nums[i]} X ${nums[j]} = ${result}`);
    }
}
