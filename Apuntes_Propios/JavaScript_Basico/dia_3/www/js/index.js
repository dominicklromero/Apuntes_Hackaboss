/* Bucle FOR */

for (let contador = 10; contador >= 0; contador -= 2) {
    console.log(contador);
}

/* Bucle WHILE */

let contador = 0;
while (contador < 10) {
    console.log(contador);
    contador++;
}
