/* #############################
 * ## Calculadora con función ##
 * #############################
 */

'use strict';

function calculator(num1, num2, option) {
    if (option === 'suma') {
        return num1 + num2;
    } else if (option === 'resta') {
        return num1 - num2;
    } else if (option === 'multiplicacion') {
        return num1 * num2;
    } else if (option === 'division' && num1 !== 0 && num2 !== 0) {
        return num1 / num2;
    } else if (option === 'potencia') {
        return num1 ** num2;
    } else {
        throw new Error('Faltan parametros o opcion incorrecta');
    }
}

console.log(calculator(5, 89, 'suma'));
console.log(calculator(54, 9, 'resta'));
console.log(calculator(889, 2, 'multiplicacion'));
console.log(calculator(90, 4, 'division'));
console.log(calculator(4, 12, 'potencia'));

const calcVar = function calculator2(num3, num4, option2) {
    switch (option2) {
        case 'suma':
            return num3 + num4;
        case 'resta':
            return num3 - num4;
        case 'multiplicacion':
            return num3 * num4;
        case 'division':
            if (num3 === 0 || num4 === 0) {
                throw new Error('No se puede dividir entre 0');
            } else {
                return num3 / num4;
            }
        case 'potencia':
            return num3 ** num4;
        default:
            throw new Error('Faltan parametros o operacion incorecta');
    }
};

console.log(calcVar(5, 78, 'suma'));
