/* ################
 * ## Pirámide 6 ##
 * ################
 *
 * Utiliza el bucle "for" para crear las siguiente figura con asteriscos (*). Debe existir una
 * variable que permita modificar la altura de la pirámide. Para el ejemplo expuesto a
 * continuación se ha tomado como referencia una altura de 4:
 *
 * - Figura:
 *
 *        *
 *       ***
 *      *****
 *     *******
 *      *****
 *       ***
 *        *
 */

'use strict';

const numFilas = Number(prompt('Altura de la piramide?'));

//Piramide Arriba

/* for (let lineas = 0; lineas <= numFilas; lineas++) {
    let fila = '';
    for (let j = 0; j < numFilas - lineas; j++) {
        fila += ' ';
    }
    for (let k = 0; k <= lineas; k++) {
        fila += '* ';
    }
    console.log(fila);
}

//Piramide Abajo
for (let i = numFilas; i > 0; i--) {
    let fila = '';
    for (let k = numFilas - i; k > 0; k--) {
        fila += ' ';
    }

    for (let m = i; m > 0; m--) {
        fila += ' *';
    }
    console.log(fila);
} */

//Parte Arriba

for (let i = 0; i < numFilas; i++) {
    let fila = '';

    //Espacios
    for (let spaces = numFilas - i - 1; spaces > 0; spaces--) {
        fila += ' ';
    }

    //Asteriscos_1
    for (let asterisk1 = 2 * i + 1; asterisk1 > 0; asterisk1--) {
        fila += '*';
    }
    console.log(fila);
}

//Parte Abajo

for (let i = numFilas - 1; i > 0; i--) {
    let fila = '';

    //Espacios
    for (let spaces = numFilas - i; spaces > 0; spaces--) {
        fila += ' ';
    }

    //Asteriscos_1
    for (let asterisk1 = 2 * i - 1; asterisk1 > 0; asterisk1--) {
        fila += '*';
    }
    console.log(fila);
}
