/**
 * ################
 * ## Pirámide 3 ##
 * ################
 *
 * Crea una función que reciba una altura y dibuje una figura
 * como la que sigue:
 *
 *    1
 *    22
 *    333
 *    4444
 *    55555
 *
 */
'use strict';

let height = Number(prompt('Numero de filas:'));

for (let lineas = 0; lineas < height; lineas++) {
    let fila = '';
    for (let nums = lineas + 1; nums > 0; nums--) {
        fila += lineas + 1;
    }
    console.log(fila);
}
