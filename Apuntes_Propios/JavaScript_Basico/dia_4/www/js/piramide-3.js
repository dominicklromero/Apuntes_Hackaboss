/**
 * ################
 * ## Pirámide 3 ##
 * ################
 *
 * Crea una función que reciba una altura y dibuje una figura
 * como la que sigue:
 *
 *    1
 *    12
 *    123
 *    1234
 *    12345
 *
 */

'use strict';

let numFilas = Number(prompt('Dime el numero de filas que deseas:'));
let fila = '';

for (let i = 0; i < numFilas; i++) {
    fila += i + 1;
    console.log(fila);
}

/* Otra forma */

/*

'use strict';

let numFilas = Number(prompt('Dime el numero de filas que deseas:'));

for (i = 0; i < numFilas; i++) {
    let fila = '';
    for (let nums = 0; nums <= i; nums++) {
        fila += nums + 1;
    }
    console.log(fila);
}

*/

/* Otra forma */

/* 'use strict';

let numFilas = Number(prompt('Dime el numero de filas que deseas:'));
let fila = '';

for (let i = 1; i <= numFilas; i++) {
    fila += i;
    console.log(fila);
} */
