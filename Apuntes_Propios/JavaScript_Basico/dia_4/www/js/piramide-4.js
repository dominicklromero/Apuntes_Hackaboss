/* ################
 * ## Pirámide 4 ##
 * ################
 *
 * Utiliza el bucle "for" para crear las siguiente figura con asteriscos (*). Debe existir
 * una variable que permita modificar la altura de la pirámide. Para el ejemplo expuesto a
 * continuación se ha tomado como referencia una altura de 4:
 *
 * - Figura:
 *
 *        *
 *       **
 *      ***
 *     ****
 */

'use strict';

let numFilas = Number(prompt('Dime el numero de filas que deseas:'));

for (let i = 0; i < numFilas; i++) {
    let fila = '';
    // Espacios
    for (let spaces = numFilas - i - 1; spaces > 0; spaces--) {
        fila += ' ';
    }
    //Asteriscos
    for (let asterisk = i + 1; asterisk > 0; asterisk--) {
        fila += '*';
    }

    console.log(fila);
}
