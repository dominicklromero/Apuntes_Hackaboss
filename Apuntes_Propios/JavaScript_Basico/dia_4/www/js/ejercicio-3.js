/* #######################
 * ## Bomba con función ##
 * #######################
 */

'use strict';

function numRandom() {
    return Number(Math.round(Math.random() * 10));
}

function bombRandom(intentos, pass) {
    for (let i = 1; i <= intentos; i++) {
        const numUser = Number(
            prompt(`Nº de intentos: ${intentos + 1 - i} || Dime un numero:`)
        );
        if (numUser === pass) {
            return true;
        }
    }
    return false;
}

if (bombRandom(5, numRandom())) {
    alert('Bomba desactivada');
} else {
    alert('BIP BIP LA BOMBA HA EXPLOTADO');
}
