'use strict';

/* function sayHello(name = 'Por Defecto') {
    //Si se iguala se pone un valor por defecto, en caso de que no se definan los parametros.
    return `El nombre es ${name}`;
}

console.log(sayHello('Alejandro')); */

/**
 *
 * @param {Number} a Primer numero
 * @param {Number} b Segundo numero                             //Esto es solo para documentar la funcion.
 * @param {String} option  Tipo de operacion (+,-,* o /)
 */

function calculate(a, b, option) {
    if (option === '+') {
        return a + b;
    } else if (option === '-') {
        return a - b;
    } else if (option === '*') {
        return a * b;
    } else if (option === '/') {
        return a / b;
    } else {
        throw new Error('No se reconoce ninguna operacion');
    }
}

console.log(calculate(3, 3, '+'));
console.log(calculate(3, 3, '-'));
console.log(calculate(3, 3, '*'));
console.log(calculate(3, 3, '/'));
console.log(calculate(3, 3, ''));
