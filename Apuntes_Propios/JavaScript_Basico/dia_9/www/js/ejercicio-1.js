/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea el objeto persona con las propiedades name y age. A mayores, crea un método que reciba un
 * array de animales y genere una nueva propiedad favoriteAnimals que almacene dicho array.
 *
 * Crea un segundo método que permita modificar cualquier propiedad del objeto. Este método debe
 * recibir dos argumentos, el nombre de la propiedad en formato string, y el valor que queremos
 * meter en la misma.
 *
 */

'use strict';

const persona = {
    name: 'Alejandro',
    age: 25,
    animals: function (array) {
        this.favAnimals = array;
    },
    modObject: function (property,value) {
        this[property] = value;  // El property si lo ponemos manual en vez de pasado como argumento de una funcion va entre comillas
    },
}

persona.animals(['Perro','Gato','Tortuga']);
persona.modObject('favColor','Azul Celeste');

console.log(persona);