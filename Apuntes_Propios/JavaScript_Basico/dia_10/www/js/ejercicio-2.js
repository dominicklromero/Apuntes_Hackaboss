/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Crea un nuevo array cuyos elementos sean los mismos que los del array original
 * pero sumándoles 10.
 *
 */

'use strict';

const nums = [0, 23, 21, 13, 100];

const suma10 = nums.map((value) => value + 10);

console.log(suma10);

// Otra forma

const array2 = [];

for (let i = 0; i < nums.length; i++) {
    array2.push(nums[i]+10)
}

console.log(array2);
