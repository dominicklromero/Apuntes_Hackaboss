/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 *   1. Obtén todos los coches de la marca Audi.
 *
 *   2. Obtén una lista con todos los colores de los coches de marca BMW.
 *
 *   3. Obtén la media de precio de los coches de marca Ford.
 *
 *   4. Obtén un array con las distintas marcas de coches (no se pueden repetir).
 *
 *   5. Obtén un array con los coches de transmisión manual y de color negro.
 *
 *   6. Obtén la suma total de todos los precios.
 *
 */

'use strict';

const coches = [
    {
        marca: 'BMW',
        modelo: 'Serie 3',
        year: 2012,
        precio: 30000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A4',
        year: 2018,
        precio: 40000,
        puertas: 4,
        color: 'Negro',
        transmision: 'automatico',
    },
    {
        marca: 'Ford',
        modelo: 'Mustang',
        year: 2015,
        precio: 20000,
        puertas: 2,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A6',
        year: 2010,
        precio: 35000,
        puertas: 4,
        color: 'Negro',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 5',
        year: 2016,
        precio: 70000,
        puertas: 4,
        color: 'Rojo',
        transmision: 'automatico',
    },
    {
        marca: 'Mercedes Benz',
        modelo: 'Clase C',
        year: 2015,
        precio: 25000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Chevrolet',
        modelo: 'Camaro',
        year: 2018,
        precio: 60000,
        puertas: 2,
        color: 'Rojo',
        transmision: 'manual',
    },
    {
        marca: 'Ford',
        modelo: 'Mustang',
        year: 2019,
        precio: 80000,
        puertas: 2,
        color: 'Rojo',
        transmision: 'manual',
    },
    {
        marca: 'Dodge',
        modelo: 'Challenger',
        year: 2017,
        precio: 40000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A3',
        year: 2017,
        precio: 55000,
        puertas: 2,
        color: 'Negro',
        transmision: 'manual',
    },
    {
        marca: 'Dodge',
        modelo: 'Challenger',
        year: 2012,
        precio: 25000,
        puertas: 2,
        color: 'Rojo',
        transmision: 'manual',
    },
    {
        marca: 'Mercedes Benz',
        modelo: 'Clase C',
        year: 2018,
        precio: 45000,
        puertas: 4,
        color: 'Azul',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 5',
        year: 2019,
        precio: 90000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Ford',
        modelo: 'Mustang',
        year: 2017,
        precio: 60000,
        puertas: 2,
        color: 'Negro',
        transmision: 'manual',
    },
    {
        marca: 'Dodge',
        modelo: 'Challenger',
        year: 2015,
        precio: 35000,
        puertas: 2,
        color: 'Azul',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 3',
        year: 2018,
        precio: 50000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 5',
        year: 2017,
        precio: 80000,
        puertas: 4,
        color: 'Negro',
        transmision: 'automatico',
    },
    {
        marca: 'Mercedes Benz',
        modelo: 'Clase C',
        year: 2018,
        precio: 40000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A4',
        year: 2016,
        precio: 30000,
        puertas: 4,
        color: 'Azul',
        transmision: 'automatico',
    },
];

// Apartado_1

const marcaAudi = coches.filter(function (coche) {
    return coche.marca === 'Audi';
});

console.log(marcaAudi);

// Apartado_2

const bmw = coches.filter((coche) => coche.marca === 'BMW');

const colores = bmw.map((color) => {
    return color.color;
});

console.log(colores);

// Apartado_3

const ford = coches.filter((coche) => coche.marca === 'Ford');

const prices = ford.map((price) => price.precio);

let total = 0;

for (let i = 0; i < prices.length; i++) {
    total += prices[i];
}

const media = total / prices.length;

console.log(`La media de los precios de los coches Ford es: ${media}`);

// Apartado_4

const marcas = coches.map((marca) => {
    return marca.marca;
})

/* const exSet = new Set(marcas);

const resultSet = [...exSet];       //Otra forma de hacer con el Set() que devuelve un objeto y por eso se usa los []...array];

console.log(resultSet); */

const marcasSin_Repetir = marcas.filter((marca2,index) => {
    return marcas.indexOf(marca2) === index;
});

console.log(marcasSin_Repetir);

//Apartado_5 

const transmisionColor = coches.filter((filter) => filter.transmision === 'manual' && filter.color === 'Negro').map((coche) => coche.marca);

console.log(transmisionColor);

// Apartado_6

const precios = coches.map((value) => value.precio);

console.log(precios);

let total2 = 0;

for (const precio of precios) {
    total2 += precio;
}

console.log(`La suma total de todos los precios es ${total2}€`);