'use strict';

/**
 *  =======================
 *  ··· P E R S O N A S ···
 *  =======================
 */
const persons = [
    {
        name: 'Pedro',
        age: 35,
        code: 'ES',
        infected: true,
        petName: 'Troski',
    },
    {
        name: 'Elisabeth',
        age: 14,
        code: 'UK',
        infected: true,
        petName: 'Firulais',
    },
    {
        name: 'Pablo',
        age: 25,
        code: 'ES',
        infected: false,
        petName: 'Berritxu',
    },
    {
        name: 'Angela',
        age: 18,
        code: 'DE',
        infected: false,
        petName: 'Noodle',
    },
    {
        name: 'Boris',
        age: 50,
        code: 'UK',
        infected: true,
        petName: 'Leon',
    },
    {
        name: 'Donald',
        age: 69,
        code: 'US',
        infected: false,
        petName: 'Pence',
    },
    {
        name: 'Pepito',
        age: 36,
        code: 'ES',
        infected: false,
        petName: 'Carbón',
    },
];

/**
 *  =======================
 *  ··· M A S C O T A S ···
 *  =======================
 */
const pets = [
    {
        petName: 'Troski',
        type: 'perro',
    },
    {
        petName: 'Firulais',
        type: 'perro',
    },
    {
        petName: 'Berritxu',
        type: 'loro',
    },
    {
        petName: 'Noodle',
        type: 'araña',
    },
    {
        petName: 'Leon',
        type: 'gato',
    },
    {
        name: 'Pence',
        type: 'perro',
    },
    {
        petName: 'Carbón',
        type: 'gato',
    },
];

/**
 *  =======================
 *  ··· A N I M A L E S ···
 *  =======================
 */
const animals = [
    {
        type: 'perro',
        legs: 4,
    },
    {
        type: 'araña',
        legs: 8,
    },
    {
        type: 'gato',
        legs: 4,
    },
    {
        type: 'loro',
        legs: 2,
    },
    {
        type: 'gallina',
        legs: 2,
    },
];

/**
 *  ===================
 *  ··· P A Í S E S ···
 *  ===================
 */
const countries = [
    {
        code: 'CN',
        name: 'China',
        population: 1439,
        infected: 81999,
    },
    {
        code: 'US',
        name: 'Estados Unidos',
        population: 331,
        infected: 112468,
    },
    {
        code: 'DE',
        name: 'Alemania',
        population: 83,
        infected: 56202,
    },
    {
        code: 'ES',
        name: 'España',
        population: 46,
        infected: 72248,
    },
    {
        code: 'UK',
        name: 'Reino Unido',
        population: 67,
        infected: 17301,
    },
];

/**
 *  ###########################
 *  ## E J E R C I C I O   1 ##
 *  ###########################
 *
 *  Número total de infectados del array de personas.
 * 
 * 
 *
 */

const infectados = persons.reduce((acc,value) => {
    if (value.infected) {
        acc++;  // acc + 1
        return acc; 
    } else {
        return acc;
    }
},0);

console.log(infectados);

/**
 *  ###########################
 *  ## E J E R C I C I O   2 ##
 *  ###########################
 *
 *  Número total de infectados en el array de países.
 *
 */

const infecCountries = countries.reduce((acc,value) => {
    acc += value.infected;
    return acc;
}, 0);

console.log(infecCountries);

/**
 *  ###########################
 *  ## E J E R C I C I O   3 ##
 *  ###########################
 *
 *  País con más infectados.
 *
 */

const uno = countries.reduce( (acc, valor) => {
    if(valor.infected > acc.infected) {
        acc = valor;
    }
    return acc;
});
console.log(uno);


/**
 *  ###########################
 *  ## E J E R C I C I O   4 ##
 *  ###########################
 *
 *  Array con el nombre de todas las mascotas.
 *
 */

const nombreAn = pets.map((valor) => {
    if (valor.type !== undefined)return valor.type;
    else return valor.name;
});

console.log(nombreAn);

/* const petNames = pets.map((pet) => pet.petName || pet.name);    //Otra forma

console.log(petNames); */


/**
 *  ###########################
 *  ## E J E R C I C I O   5 ##
 *  ###########################
 *
 *  Array de españoles con perro.
 *
 */

 const espanolesConPerro = persons.filter((person) =>
 {
     return pets.find((animal) =>{
         return (((person.petName === animal.type) || (person.petName === animal.name)) && person.code == 'ES' && animal.type == 'perro');
     });
 });
 console.log(espanolesConPerro);
 
/**
 *  ###########################
 *  ## E J E R C I C I O   6 ##
 *  ###########################
 *
 *  Array con las personas. A mayores, este array debe incluír el objeto con los datos de su mascota
 *
 *  {
 *      name: 'Pedro',
 *      age: 35,
 *      country: 'ES',
 *      infected: true,
 *      pet: {
 *          name: 'Troski',
 *          type: 'perro',
 *      }
 *  }
 *
 */

 const personsWithAnimal = persons.map((person)=>{

    return {
        name: person.name,
        age: person.age,
        country: person.code,                  // Se puede usar el spread ...person
       infected: person.infected,
       pet: pets.find( (animal) => {
           return ((person.petName === animal.type) || (person.petName === animal.name));
        }),
    };
});

console.log(personsWithAnimal);


/**
 *  ###########################
 *  ## E J E R C I C I O   7 ##
 *  ###########################
 *
 *  Número total de patas de las mascotas de las personas.
 *
 */

const numberOfLegs = persons.reduce((acc,person) => {
    pets.find((pet) => {
        (pet.petName === person.petName || pet.name === person.petName) && animals.find((animal) => {
            if (animal.type === pet.type) {
                acc += animal.legs;
            }
        });
    });
    return acc;
},0);

console.log(numberOfLegs);

/**
 *  ###########################
 *  ## E J E R C I C I O   8 ##
 *  ###########################
 *
 *  Array con las personas que tienen animales de 4 patas
 *
 */

const pet4legs = persons.filter((person) => {
    return pets.find((pet) => {
        return animals.find((animal) => {
            return (person.petName === pet.name || person.petName === pet.petName) && pet.type === animal.type && animal.legs === 4;
        })
    })
})

console.log(pet4legs);

/**
 *  ###########################
 *  ## E J E R C I C I O   9 ##
 *  ###########################
 *
 *  Array de países que tienen personas con loros como mascota.
 *
 */

'use strict';

const countryLoro = countries.filter((country) => {
    return persons.find((person) => {
        return pets.find((pet) => {
            return country.code === person.code && (pet.petName === person.petName || pet.name === person.petName) && pet.type === 'loro';
        })
    })
});

console.log(countryLoro.map((country) => country.name));

/**
 *  #############################
 *  ## E J E R C I C I O   1 0 ##
 *  #############################
 *
 *  Número de infectados totales (en el array de países) de los países con mascotas de ocho patas.
 *
 */

'use strict';

const infectedTotal = countries.filter((country) => {
    return persons.find((person) => {
        return pets.find((pet) => {
            return animals.find((animal) => {
                return country.code === person.code && (person.petName === pet.petName || person.petName === pet.name) &&
                    pet.type === animal.type && animal.legs === 8;
            });
        });
    });
});

console.log(infectedTotal.map((infected) => `${infected.name} con ${infected.infected} infectados`));