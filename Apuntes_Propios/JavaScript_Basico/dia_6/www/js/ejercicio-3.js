/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dados el siguiente array: [true, false, false, false, true, false, false, false, true]
 *
 *  - Los valores positivos son infectados.
 *
 *  - Los infectados transmiten su enfermedad a los no infectados que tienen a los lados.
 *
 * En este caso el resultado debería ser: [true, true, false, true, true, true, false, true, true];
 *
 */

'use strict';

const booleans1 = [true, false, false, false, true, false, false, false, true];
// Con booleans2 = [...booleans1]; seria lo mismo. Copiar el contenido.
const booleans2 = [true, false, false, false, true, false, false, false, true];


for (let i = 0; i < booleans1.length; i++) {
    if (booleans1[i]) {
        if (i > 0) {
            booleans2[i - 1] = true
        }

        if (i < booleans1.length - 1) {
            booleans2[i + 1] = true;
        }
    }
}

console.log(booleans2);