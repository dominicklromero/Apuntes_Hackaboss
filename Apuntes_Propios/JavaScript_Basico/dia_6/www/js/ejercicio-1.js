/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Dado el array [3, 4, 13, 5, 6, 8], muestra por consola qué numeros son pares
 * y qué números son impares.
 *
 * Haz lo mismo pero en este caso indica qué números son primos y cuales no.
 *
 * Por último, crea un nuevo array en el que los valores sean el doble del array
 * original.
 *
 */

'use strict';

const nums = [3, 4, 13, 5, 6, 8];

for (let i = 0; i < nums.length; i++) {
    let resto = nums[i]%2;
    if ( resto === 0) {
        console.log(`El nº ${nums[i]} es PAR`);
    } else {
        console.log(`El nº ${nums[i]} es IMPAR`);
    }
}

console.log('########################');

function isPrime(num) {
    for (let i = 2; i < num; i++) {
        if(num % i === 0) {
            return false;
        }
        return true;
    }
}

for (const num of nums) {
    if (isPrime(num)) {
        console.log(`${num} es primo`);
    } else {
        console.log(`${num} no es primo`);

    }
}


console.log('########################');

const nums2 = [];

for (const numero of nums) {
    nums2.push(numero*2);
}

console.log(nums2);