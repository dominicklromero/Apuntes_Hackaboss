/* #################
 * ## Ejercicio 5 ##
 * #################
 *
 * - Cuenta el número de letras "r" en el siguiente fragmento de texto:
 *   "Tres tristes tigres tragan trigo en un trigal."
 *
 * - Ahora cuenta las "t". Debes contar las mayúsculas y las minúsculas.
 *
 * - Sustituye todas las "e" por "i".
 *
 */

'use strict';

const exampText = 'Tres tristes tigres tragan trigo en un trigal.';

const arrayR = exampText.split('');
let contador = 0;

for (let i = 0; i < arrayR.length; i++) {
    if(arrayR[i] === 'r') {
        contador++;
    }
}

console.log(contador);

console.log('#####################################');

const arrayT = exampText.split('');
let contador2 = 0;

for (let j = 0; j < arrayT.length; j++) {
    if (arrayT[j] === 't' || arrayT[j] === 'T') {
        contador2++;
    }
}

console.log(contador2);

console.log('#####################################');

const sustE_I = exampText.replaceAll('e','i');
console.log(sustE_I);