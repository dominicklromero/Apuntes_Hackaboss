/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea un programa que compruebe si un string es palíndromo, es decir, que se lee igual
 * del derecho que del revés. Por ejemplo, "Arriba la birra" es un palíndromo.
 *
 */

'use strict';

const derText = 'Arriba la birra';
const xSpaces = derText.replaceAll(' ', '').toLowerCase();
const revText = xSpaces.split('').reverse().join('');

if (xSpaces === revText) {
    console.log(`El string '${derText}' es palindromo`);
} else {
    console.log(`El string '${derText}' no es palindromo`);
}
