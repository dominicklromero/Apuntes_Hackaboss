/**
 * #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea una función que pida una cadena de texto y la devuelva al revés.
 * Es decir, si tecleo "hola que tal" deberá mostrar "lat euq aloh".
 *
 */

'use strict';

function reverseStr(string) {
    const cadenaTx = string;
    const reverseTX = cadenaTx.split('').reverse().join('');
    console.log(reverseTX);
}

reverseStr('Hola, soy Alejandro y tengo 25 años');
