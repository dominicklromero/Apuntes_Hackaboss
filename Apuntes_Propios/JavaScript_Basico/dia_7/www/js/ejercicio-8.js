/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Ordena un array de números de menor a mayor sin usar la función sort().
 *
 */

'use strict';

const nums = [4, 10, 7, 1, 2];

/* nums.splice(0,nums.length,1,4,8,12,19); */  //No es lo mas adecuado.

console.log(nums);

let temp;

for (let i = 0; i < nums.length; i++) {
    for (let x = 0; x < (nums.length - i); x++) {
        if (nums[x] > nums[x + 1]) {
            temp = nums[x];
            nums[x] = nums[x + 1];
            nums[x + 1] = temp
        }
    }
}

console.log(nums);