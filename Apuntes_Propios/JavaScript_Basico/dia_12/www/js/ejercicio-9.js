/**
 * #################
 * ## Ejercicio 9 ##
 * #################
 *
 * Dada la función "stringScramble(string1, string2)" toma los dos parámetros y devuelve true
 * si una parte de los caracteres de string1 se pueden reordenar para que coincidan con los
 * de string2, en caso contrario devuelve false.
 *
 * Por ejemplo: si string1 es "rkqodlw" y string2 es "world" la salida debería devolver true.
 *
 */

'use strict';

const string1 = 'rkqodlw';
const string2 = 'world';

function stringScramble(string1,string2) {
    const array1 = string1.split('');

    let contador = 0;

    for (const letter2 of string2) {
        for (let i = 0; i < array1.length; i++) {
            if (letter2 === array1[i]) {
                array1.splice(i,1);
                contador++;
                break;
            }
        }
    }

    if (string2.length === contador) {
        return true;
    } else {
        return false;
    }
}

console.log(stringScramble(string1,string2));