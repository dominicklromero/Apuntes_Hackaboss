/*
 * ####################
 * ##  Ejercicio 10  ##
 * ####################
 *
 * 1. Crea el objeto "fox" y el objeto "chicken" a partir de la función "newAnimal". Ambos objetos
 *    tendrán la propiedad "type", que será "zorro" o "gallina", y el método "breed". Este método
 *    devuelve un nuevo zorro o una nueva gallina (en función del animal).
 *
 * 2. A mayores, el zorro tiene el método "goLunch" que recibe un array de animales como parámetro,
 *    busca una gallina en el array y la elimina.
 *
 * 3. Crea una función que devuelva un nuevo array con dos animales al azar. A la función debemos
 *    pasarle un objeto zorro y un objeto gallina como argumentos. Este array debe retornar un array
 *    con una de las siguientes combinaciones:
 *
 *   - 2 zorros.
 *
 *   - 2 gallinas.
 *
 *   - 1 gallina / 1 zorro.
 *
 *
 * 4. Crea otra función que reciba un array con dos animales y haga lo siguiente:
 *
 *   - Si en el array hay 2 animales del mismo tipo utilizamos la función breed para incorporar un tercer
 *     animal en el array, retornando como resultado un array con tres animales iguales.
 *
 *   - Si en el array hay un animal de cada el zorro se come a la gallina.
 *
 */

function newAnimal(type) {

    return {
        type,
        breed : function() {
            return this;
        },
    };
}

const Fox = newAnimal('zorro');
console.log(Fox);

const Chicken = newAnimal('gallina');
console.log(Chicken);

const newFox = Fox.breed();
console.log(newFox);

const newChicken = Chicken.breed();
console.log(newChicken);

Fox.goLaunch = function(arrAnimals) {
    
    let chickenIndex = null;

    for (let i = 0; i < arrAnimals.length; i++) {
        if (arrAnimals[i].type === 'gallina') {
            chickenIndex = i;
        }
    }

    arrAnimals.splice(chickenIndex,1);

    return arrAnimals;
}

console.log(Fox.goLaunch([Fox,Chicken]));


function generateAnimals(foxObj,chickenObj) {
    const animals = [];

    for (j = 0; j < 2; j++) {
        const random = Math.floor(Math.random() * 2);

        if (random) {
            animals.push(foxObj);
        } else {
            animals.push(chickenObj);
        }
    }

    return animals;
}

console.log(generateAnimals(Fox,Chicken));


function playGame(animales) {
    if (animales[0].type === animales[1].type) {
        const breed = animales[0].breed();
        animales.push(breed);
    } else {
        let foxIndex = null;

        for (let i = 0; i < animales.length; i++) {
            if (animales[i].type === 'zorro') {
                foxIndex = i;
            }
        }

        animales[foxIndex].goLaunch(animales);
    }

    return animales;
}

const animalList = generateAnimals(Fox,Chicken);

const result = playGame(animalList);

console.log(result);