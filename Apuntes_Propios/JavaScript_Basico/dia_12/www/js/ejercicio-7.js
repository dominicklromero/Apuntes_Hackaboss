/**
 * #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Crea un nuevo array con los amigos en común que tienen dos usuarios de Instagram. El ejercicio
 * debe ser resuelto sin utilizar ningún método excepto push.
 *
 */

'use strict';

const pacosFollowers = ['Manolo', 'Marta', 'Pablo', 'Kevin'];

const luciasFollowers = ['Pablo', 'Marta', 'Ana'];

const comun = [];

for (let i = 0; i < pacosFollowers.length; i++) {
    for (let k = 0; k < luciasFollowers.length; k++) {
        if (pacosFollowers[i] === luciasFollowers[k]) {
            comun.push(pacosFollowers[i]);
        }
    }
}

console.log(comun);

