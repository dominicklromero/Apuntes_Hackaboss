/**
 * #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Dada la función "longestWord(wordsArray)" tome el parámetro wordsArray y devuelve la palabra
 * más larga del array.
 *
 * Si hay dos o más palabras que tienen la misma longitud, devuelve la primera palabra.
 *
 */

'use strict';

const wordArray = ['hola','adios','estrella','estadio','Alejandro','montaña','playa'];

function longestWord(words) {
    let longestString = '';
    for (const word of words) {
      if (word.length > longestString.length) {
        longestString = word;
      }
    }
    return longestString;
  }
  
  console.log(longestWord(wordArray));
  


