/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Dado el siguiente tablero (un array de arrays) haz las modificaciones necesarias para
 * lograr esto:
 *
 *     - Figura 1:
 *
 *      [
 *          ['X', '-', '-', '-', 'X'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['-', '-', 'X', '-', '-'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['X', '-', '-', '-', 'X']
 *      ];
 *
 *
 *     - Figura 2:
 *
 *      [
 *          ['X', 'X', 'X', 'X', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', 'X', 'X', 'X', 'X']
 *      ];
 *
 */

'use strict';

const board_1 = [
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
];

const board_2 = [
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
];

for (let i = 0; i < board_1.length; i++) {
    board_1[i][i] = 'X';
    board_1[i][board_1.length - 1 - i] = 'X';
}

for (let j = 0; j < board_2.length; j++) {
    for (let k = 0; k < board_2.length; k++) {
        if (j === 0 || j === board_2.length - 1) {
            board_2[j][k] = 'X';
        }
    }
    board_2 [j][0] = 'X';
    board_2 [j][board_2.length - 1] = 'X'
}

console.log(board_1);
console.log(board_2);
