/**
 * #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Dada la función "timeConvert(minutes)" toma el parámetro minutes y devuelve el número
 * de horas y minutos a los que el parámetro se convierte (es decir, si minutes = 63 entonces
 * la salida debería ser 1:03). Separe el número de horas y minutos con dos puntos.
 *
 */

'use strict';

//Primera forma

function timeConvert(minutes) {
  let hour = 0;

  for (minutes; minutes >= 60; minutes -= 60) {
    hour++;
  }

  minutes = (minutes < 10) ? '0' + minutes : minutes;
  
  console.log(`${hour} : ${minutes}`);
}
console.log(timeConvert(63));

//Segunda forma

function timeConverter(minutes) {
    const num = minutes;
    const hours = Math.floor(num / 60);
    let mins = Math.round((num / 60 - hours) * 60);
    if (mins < 9) {
      mins = '0' + mins.toString();
    }
    return hours.toString() + ':' + mins;
}
  
console.log(timeConverter(63));

//Tercera forma

function timeConverter2(minutes) {
    const num = minutes;
    const hours = Math.floor(num / 60);
    let mins = Math.round((num / 60 - hours) * 60);
    if (mins <= 9) {
      mins = '0' + mins;
    }
    return `${hours}:${mins}`;
  }
  
  console.log(timeConverter2(500));
  