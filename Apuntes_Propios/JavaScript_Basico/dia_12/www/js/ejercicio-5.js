/**
 * #################
 * ## Ejercicio 5 ##
 * #################
 *
 * Dada la función "alphabetSoup(string)" tome el parámetro string y devuelve la cadena con las
 * letras en orden alfabético (es decir, "hola" se convierte en "ehllo").
 *
 */

'use strict';

const cadena = 'HoLa';

function alphabetSoup(string) {
    const result = string.toLowerCase().split('').sort().join('');
    return result;
}

console.log(alphabetSoup(cadena));
