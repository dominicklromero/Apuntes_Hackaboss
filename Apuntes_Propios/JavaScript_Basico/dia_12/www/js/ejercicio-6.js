/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 * Dada la función "simpleMode(numsArray)" tome el parámetro numsArray y devuelva el número
 * que aparece con más frecuencia.
 *
 * Por ejemplo: si numsArray contiene [10, 4, 5, 2, 4] la salida debería ser 4. Si hay existen
 * dos números que se repiten el mismo número de veces devuelve el que apareció primero en el
 * array (por ejemplo [5, 10, 10, 6, 5] debería devolver 5 porque apareció primero).
 *
 * Si no hay ningún modo, devuelve -1. El array no estará vacío.
 *
 */

'use strict';

const numsArray = [5, 10, 10, 10, 6, 5, 10];

function simpleMode(numsArray){
    let moda = -1;
    let repeticionesModa = 0;

    for (let i = 0 ; i<numsArray.length ; i++){
        let repeticiones = 0;
        for (let j = 0 ; j<numsArray.length ; j++) if(numsArray[i]===numsArray[j]) repeticiones++;
        if (repeticiones>1 && repeticiones>repeticionesModa) {
            moda = numsArray[i];
            repeticionesModa = repeticiones;
        }
    }
    return moda;
}
console.log(simpleMode(numsArray));


function simpleMode2(numsArray){
    let mode = null;
    let countCurrentNum = 0;
    let countMode = 0;

    for (let i = 0; i < numsArray.length; i++) {
        for (let j = i + 1; j < numsArray.length; j++) {
            if (numsArray[i]===numsArray[j]) {
                countCurrentNum++;
            }
        }
        if (countCurrentNum > countMode) {
            mode = numsArray[i];
            countMode = countCurrentNum;
            countMode = 0;
        }
    }

    if (mode === null) {
        console.log(-1);
    } else {
        console.log(mode);
    }
}
console.log(simpleMode2(numsArray));