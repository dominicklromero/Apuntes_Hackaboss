/**
 * Ejemplo de la estructura final:
 *
 *  <ul id="tweets">
 *      <li>
 *          <p>Lo que escriba el usuario en el input.</p>
 *          <footer>
 *              <time>23/3/2021</time>
 *              <button class="action">Borrar</button>
 *          </footer>
 *      </li>
 *      <li>
 *          <p>Lo que escriba el usuario en el input.</p>
 *          <footer>
 *              <time>23/3/2021</time>
 *              <button class="action">Borrar</button>
 *          </footer>
 *      </li>
 *      <li>
 *          (...)
 *      </li>
 *  </ul>
 */

const buttonForm = document.querySelector('form > button.action');
const ul = document.querySelector('ul#tweets');
const form = document.forms.twitter;
const buttonLi = document.querySelector('footer > button.action');

function currentDate() {
    const current = new Date();
    const options_A = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
    };
    const option_B = {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
    };
    const data = current.toLocaleDateString('es-ES', options_A);
    const time = current.toLocaleTimeString('es-ES', option_B);

    return `${data} // ${time}`;
}

const publicaTweet = (e) => {
    const frag = document.createDocumentFragment();
    const li = document.createElement('li');

    e.preventDefault();

    if (form.elements.tweet.value.length > 280) {
        alert('Has execedido el número de caracteres [Máximo = 280]');
    } else if (form.elements.tweet.value === '') {
        alert('No se permite el envío de un tweet vacío');
    } else {
        li.innerHTML = `
        <p>${form.elements.tweet.value}</p>
        <footer>
            <time>${currentDate()}</time>
            <button class="action">Borrar</button>
        </footer>
        `;

        frag.append(li);
        ul.append(frag);
        form.elements.tweet.value = '';
    }
};

const borraTweet = (e) => {
    const { target } = e;

    if (target.matches('button.action')) {
        const li = document.querySelector('li');
        li.remove();
    }
};

buttonForm.addEventListener('click', publicaTweet);
ul.addEventListener('click', borraTweet);
