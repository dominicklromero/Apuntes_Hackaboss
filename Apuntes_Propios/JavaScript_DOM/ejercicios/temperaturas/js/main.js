/**
 * Completa la tabla de temperaturas tomando como referencia
 * este array de ciudades. Tendrás que usar las siguientes
 * clases para cambiar el color de fondo de cada temperatura.
 *
 *  - lower: temp. menor que 4: fondo azul.
 *
 *  - low: temp. entre 4 y 20: fondo verde
 *
 *  - medium: temp. entre 20 y 30: fondo naranja
 *
 *  - high: temp. mayor de 30: fondo rojo
 *
 */

const temperaturas = [
    {
        city: 'A Coruña',
        min: 17,
        max: 23,
    },
    {
        city: 'Ferrol',
        min: 15,
        max: 32,
    },
    {
        city: 'Lugo',
        min: -20,
        max: 31,
    },
    {
        city: 'Ourense',
        min: 18,
        max: 35,
    },
    {
        city: 'Pontevedra',
        min: 18,
        max: 29,
    },
];

const ciudades = temperaturas.map((ciudad) => {
    return ciudad.city; // ['A Coruña','Ferrol','Lugo','Ourense','Pontevedra']
});

const tempMax = temperaturas.map((max) => {
    return max.max; // [23,32,31,35,29]
});

const tempMin = temperaturas.map((min) => {
    return min.min; // [17,15,-20,18,18]
});

const frag = document.createDocumentFragment();

for (let i = 0; i < ciudades.length; i++) {
    const tr = document.createElement('tr');
    for (let j = 1; j <= Object.keys(temperaturas[i]).length; j++) {
        let td = document.createElement('td');

        if (j === 1) {
            td.textContent = ciudades[i];
        } else if (j === 2) {
            td.textContent = tempMin[i];
        } else {
            td.textContent = tempMax[i];
        }

        if (td.textContent < 4) {
            td.classList.add('lower');
        } else if (td.textContent > 4 && td.textContent < 20) {
            td.classList.add('low');
        } else if (td.textContent > 20 && td.textContent < 30) {
            td.classList.add('medium');
        } else if (td.textContent > 30) {
            td.classList.add('high');
        }

        tr.appendChild(td);
    }
    frag.append(tr);
}

const tbody = document.querySelector('tbody');
tbody.appendChild(frag);

// Otra forma

/* for (const ciudad of temperaturas) {
    const tr = document.createElement('tr');

    tr.innerHTML = `
        <td>${ciudad.city}</td>
        <td>${ciudad.min}</td>
        <td>${ciudad.max}</td>
    `;

    // Falta pasarle las clases con los fondos de colores

    frag.append(tr);
}

tbody.append(frag); */
