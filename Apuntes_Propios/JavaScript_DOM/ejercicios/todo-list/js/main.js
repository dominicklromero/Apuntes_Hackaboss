/**
 * Ejemplo de estructura de un li (una tarea) en el HTML:
 *
 * <ul class="todo-list">
 *
 *      <li data-index="0">
 *          <input type="checkbox" />
 *          <p>Texto de la tarea</p>
 *          <time datetime="fecha-en-iso">18/11/2020 - 21:47</time>
 *      </li>
 *
 * </ul>
 */

/**
 * ##################################################
 * ##  Importa las funciones y objetos necesarios  ##
 * ##################################################
 */

import { State, addTodo, toggleTodo, deleteAllTodos, cleanTodos } from './state.js';

/**
 * ######################################
 * ##  Selección de nodos / elementos  ##
 * ######################################
 *
 *  - Selecciona el formulario.
 *
 *  - Selecciona el ul.
 *
 *  - Selecciona el botón que se encargará de limpiar las tareas marcadas
 *    como completadas.
 *
 *  - Selecciona el botón que se encargará de vaciar las tareas.
 */
const form = document.querySelector('form.todo-form');
const todoList = document.querySelector('ul.todo-list');
const todoClean = document.querySelector('button.todo-clean');
const todoEmpty = document.querySelector('button.todo-empty');

/**
 * ##############################
 * ##  Enviar una nueva tarea  ##
 * ##############################
 *
 * Agregamos una función manejadora al evento submit del formulario que se
 * encargue de agregar una nueva tarea con los datos que el usuario introdujo
 * en el input.
 *
 * Renderiza la página tras enviar una nueva tarea.
 *
 */

form.addEventListener('submit', (e) => {
    
    // Prevenimos que se envie el formulario al pulsar el boton.
    e.preventDefault();

    // Seleccionar teexto del input.
    const input = form.elements.todo;

    // Creamos una nueva tarea con el texto del input.
    addTodo(input.value);

    input.value = '';

    render();
})

/**
 * ##################################################
 * ##  Marcar tarea como realizada / no realizada  ##
 * ##################################################
 *
 * Agrega un evento al ul que marque una tarea como realizada o
 * no realizada.
 *
 * Renderiza la página tras marcar / desmarcar las tareas.
 *
 */

todoList.addEventListener('click', (e) => {
    
    // Seleccionamos el elemento que hayamos clicado
    const {target} = e;

    // Comprobamos si el elemento clicado es el checkbox
    if (target.matches('input[type="checkbox"]')) {

        // Seleccionamos el li mas cercano.
        const closestLi = target.closest('li');

        // Obtenemos el index del li
        const index = closestLi.getAttribute('data-index');

        // Cambiamos el estado de la tarea.
        toggleTodo(index);

        render();
    }
})

/**
 * #################################################
 * ##  Eliminar tareas marcadas como completadas  ##
 * #################################################
 *
 * Agrega un evento de click al botón que se encarga de limpiar
 * las tareas marcadas como completadas para eliminar estas tareas.
 *
 * Renderiza la página tras eliminar las tareas.
 *
 */

todoClean.addEventListener('click', () => {
    cleanTodos();
    render();
})

/**
 * #################################
 * ##  Eliminar TODAS las tareas  ##
 * #################################
 *
 * Agrega un evento de click al botón que se encarga de eliminar
 * todas las tareas para borrar todas las tareas.
 *
 * Renderiza la página tras eliminar las tareas.
 *
 */

todoEmpty.addEventListener('click', () => {
    if(confirm('¿Deseas eliminar todas las tareas?')) {
        deleteAllTodos();
        render();
    }
})

/**
 * #####################################
 * ##  Actualizar la lista de tareas  ##
 * #####################################
 *
 * Crea la función render que se encargará de actualizar las tareas
 * en el HTML cada vez que hagamos un cambio.
 *
 */

const render = () => {
    /**
     *  1. Vaciamos el contenido del ul.
     *
     *  2. Creamos un fragmento de documento en el cuál meteremos
     *     los lis (las tareas).
     *
     *  3. Recorremos el array de todos.
     *
     *      [¡Ojo! Puedes utilizar la propiedad innerHTML si lo deseas]
     *
     *      3.1. Creamos el li y le agregamos el atributo "data-index"
     *           con el valor del index de la tarea en el array.
     *
     *      3.2. Creamos el checkbox y le agregamos el atributo "type" con
     *           el valor "checkbox".
     *
     *      3.3. Si la tarea está marcada como realizada agregamos al li la clase
     *           "done" y al checkbox le agregamos el atributo "checked" con el
     *           valor "yes".
     *
     *      3.4. Creamos el párrafo y le agregamos el texto de la tarea.
     *
     *      3.5. Creamos el tag time y le agregamos el atributo "datetime" con el
     *          valor de la fecha en la que se creó la tarea. Recuerda formatear
     *          la fecha para que quede guay.
     *
     *      3.6. Metemos los nodos creados en el li.
     *
     *      3.7. Agregamos el li (la tarea) al fragmento.
     *
     * 4. Agregamos los nodos del fragmento al ul.
     */

    todoList.innerHTML =  '';

    const frag = document.createDocumentFragment();

    for (let i = 0; i < State.todos.length; i++) {

        const li = document.createElement('li');

        li.setAttribute('data-index', i);
        
        const currentTodo = State.todos[i];

        // Agregamos al LI la clase done si la tarea esta marcada como hecha
        if (currentTodo.done) li.classList.add('done');

        li.innerHTML = 
            `
            <input type="checkbox" ${currentTodo.done ? 'checked' : ''} >
            <p>${currentTodo.text}</p>
            <time datetime="${currentTodo.date}">${new Date(currentTodo.date).toLocaleDateString()}</time> 
            `

        frag.append(li);
    }

    todoList.append(frag);
};

render();
