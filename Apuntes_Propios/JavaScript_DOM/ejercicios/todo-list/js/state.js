/**
 * ####################
 * ##  LocalStorage  ##
 * ####################
 *
 * Obtenemos las tareas almacenadas en el Local Storage (si las hay).
 *
 */

const localStorageTodos = window.localStorage.getItem('todos');

/**
 * #############
 * ##  State  ##
 * #############
 *
 * Si en la constante anterior recibimos un valor, JavaScript se tomará
 * eso como un true (la variable NO está vacía). En este caso nos quedamos
 * con lo que haya en el localStorage.
 *
 * De lo contrario, si localStorageTodos está vacío, "todos" será igual
 * a un array vacío.
 */
const State = {
    todos: localStorageTodos ? JSON.parse(localStorageTodos) : [],
};

/**
 * ########################################################
 * ##  Almacenar el array de tareas en el Local Storage  ##
 * ########################################################
 */

const saveTodos = () => {
    // Convertimos a una cdena de texto JSON el array de todos
    const todosJSON = JSON.stringify(State.todos);

    // Guardamos el item en el localStorage
    window.localStorage.setItem('todos', todosJSON);

};

/**
 * ###############################
 * ##  Agregar una nueva tarea  ##
 * ###############################
 *
 *  newTodo = {
 *      text: text,
 *      done: false,
 *      date: "2021-05-12T15:53:18.507Z"
 *  };
 *
 * Si quieres usar el mismo formato de fecha que en el ejemplo anterior
 * puedes aplicar el método "toISOString()" sobre un objeto Date.
 *
 * + info: https://cutt.ly/5bF2Lhu
 *
 */

const addTodo = (text) => {
    // Creamos la tarea
    const newTodo = {
        text,  // Lo mismo text: text, pero se hace asi porque se llama igual.
        done: false,
        date: new Date().toISOString(),
    };

    // Pusheamos la tarea
    State.todos.unshift(newTodo);
    
    // Cuando termines guarda los cambios en el localStorage.
    saveTodos();
};


/**
 * #################################
 * ##  Eliminar todas las tareas  ##
 * #################################
 */

const deleteAllTodos = () => {

    State.todos = []; //Alternativa: State.todos.length = 0

    // Cuando termines guarda los cambios en el localStorage.
    saveTodos();
};

/**
 * ####################################################
 * ##  Marcar tarea como completada / no completada  ##
 * ####################################################
 */

const toggleTodo = (index) => {

    // Seleccionamos la tarea utilizando el index
    const todo = State.todos[index];

    // Comprobamos si la tarea existe
    if (!todo) {
        throw new Error('La tarea no existe')
    }
    
    // Si done es igual a true, lo convertivos a false y viceversa
    todo.done = !todo.done;

    // Cuando termines guarda los cambios en el localStorage.
    saveTodos();
};

/**
 * ###################################
 * ##  Eliminar tareas completadas  ##
 * ###################################
 */

const cleanTodos = () => {

    // Filtramos las tareas cuya propiedad done se false
    State.todos = State.todos.filter((todo) => !todo.done)

    // Cuando termines guarda los cambios en el localStorage.
    saveTodos();
};

/**
 * ###########################################
 * ##  Exportamos el State y las funciones  ##
 * ###########################################
 */

export { State, addTodo, toggleTodo, deleteAllTodos, cleanTodos };
