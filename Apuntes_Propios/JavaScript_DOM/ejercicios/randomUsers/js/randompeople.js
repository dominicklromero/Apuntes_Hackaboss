/**
 * Cada <li> debería tener una estructura similar a esta:
 *
 * <article>
 *      <header>
 *          <img src="${imagenUsuario}" alt="${nombreCompleto}">
 *          <h1>${nombreCompleto}</h1>
 *      </header>
 *      <p>${ciudad} (${país}), ${añoNacimiento}</p>
 * </article>
 *
 *
 * API: https://randomuser.me/api/?results=10
 */

/* const frag = document.createDocumentFragment();

const ul = document.querySelector('ul.userlist');

const apiUsers = async function (url) {
    try {
        const res = await fetch(url);
        const users = await res.json();

        console.log(users);

        for (const user of users.results) {
            const li = document.createElement('li');

            li.innerHTML = `
             <article>    
                 <header>
                     <img src="${user.picture.large}" alt="${
                user.name.first + ' ' + user.name.last
            }">
                     <h1>${user.name.first + ' ' + user.name.last}</h1>
                 </header>
                 <p>${user.location.city} (${
                user.location.country
            }), ${user.dob.date.slice(0, 4)}</p>
             </article>`;

            frag.append(li);
        }

        const liLoad = document.querySelector('.loading');
        liLoad.remove();

        ul.append(frag);
    } catch (error) {
        console.log(error);
    }
};

setTimeout(() => {
    const numUsers = Number(prompt('Numero de usuarios (Del 1 al 100)'));

    apiUsers(`https://randomuser.me/api/?results=${numUsers}`);
}, 300); */

// Mi forma

const howManyUsers = Number(prompt('Dime cuantos usuarios (Del 1 al 100):'));
const frag = document.createDocumentFragment();
const liLoad = document.querySelector('li.loading');
const ul = document.querySelector('ul.userlist');

if (howManyUsers < 1 || howManyUsers > 100 || isNaN()) {
    liLoad.textContent = 'Numero o formato incorrecto';
} else {
    liLoad.remove();

    async function getUsers(Url) {
        try {
            const res = await fetch(Url);
            const { results: users } = await res.json();

            for (const user of users) {
                const li = document.createElement('li');

                li.innerHTML = `
                    <article>
                        <header>
                            <img src="${user.picture.large}" alt="${
                    user.name.first + ' ' + user.name.last
                }">
                            <h1>${user.name.first + ' ' + user.name.last}</h1>
                        </header>
                        <p>${user.location.city} (${
                    user.location.country
                }), ${user.dob.date.slice(0, 4)}</p>
                    </article>
                    `;

                frag.append(li);
            }
            ul.appendChild(frag);
        } catch (error) {
            console.error(error);
        }
    }

    getUsers(`https://randomuser.me/api/?results=${howManyUsers}`);
}
