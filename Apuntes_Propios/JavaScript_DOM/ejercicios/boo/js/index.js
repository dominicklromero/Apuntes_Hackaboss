/**
 * #################
 * ## 👻 B O O 👻 ##
 * #################
 *
 * Haz que, cada segundo que pase, el div "boo" aparezca ubicado en una
 * posición aleatoria de la ventana y que el color del body cambie, también
 * de forma aleatoria "rgb(?, ?, ?)".
 *
 * ¡Ojo! No dejes que Boo se salga de los márgenes de la ventana, debemos
 * poder ver a Boo sin hacer scroll en ningún momento.
 *
 * element.getBoundingClientRect() --> devuelve una serie de propiedades
 * del nodo seleccionado, entre ellas, la altura y el ancho del elemento.
 *
 * Bonus point --> haz que aparezcan más emojis acompañando a Boo. Para ello
 *                 deberás crear más divs y seleccionarlos a todos. Esto
 *                 requerirá a mayores que cambies cositas en el código.
 *
 */

/* const { body } = document;
//console.log(body);
const boo = document.querySelector('.boo');
//console.log(boo);

setInterval(ourFunction, 1000);

function ourFunction() {
    boo.style.left = movX() + 'px';
    boo.style.top = movY() + 'px';
    woman.style.left = movX() + 'px';
    woman.style.top = movY() + 'px';
    body.style.backgroundColor = `rgb(${movement(256)},${movement(
        256
    )},${movement(256)})`;
}

function movX() {
    const width = window.innerWidth;
    const booWidth = boo.getBoundingClientRect().width;
    const mov = width - booWidth;
    //console.log('MOV X: ' + mov + 'BODY);
    return movement(mov);
}

function movY() {
    const height = window.innerHeight;
    const booHeight = boo.getBoundingClientRect().height;
    const mov = height - booHeight;
    //console.log('MOV Y: ' + mov);
    return movement(mov);
}

/* function movement(max) {
     return Math.floor(Math.random() * max);
 } 

const movement = (max) => {
    return Math.floor(Math.random() * max);
};

const woman = document.createElement('div');
body.appendChild(woman);
woman.classList.add('boo');
woman.textContent = '👻'; */

// Forma profesor

// Seleccionamos elementos
/* const boo = document.querySelector('div.boo'); */
const booArray = document.querySelectorAll('div');
const body = document.body;

//Funcion que genera un numero entre un maximo y un minimo
function random(max, min = 0) {
    return Math.floor(Math.random() * (max + 1 - min) + min); // Con esto generamos un numero random.
}

for (const boo of booArray) {
    // Intervalo cada 1s
    setInterval(() => {
        //Dimendiones del div.boo
        const {
            width: booWidth,
            height: booHeight,
        } = boo.getBoundingClientRect();

        //Obtenemos la altura maxima y el ancho maximo
        const maxHeight = window.innerHeight - booHeight;
        const maxWidth = window.innerWidth - booWidth;

        boo.style.cssText = `
        top: ${random(maxHeight)}px;
        left: ${random(maxWidth)}px;
        font-size: ${random(150, 80)}px;
    `;

        body.style.backgroundColor = `rgb(${random(255)}, ${random(
            255
        )}, ${random(255)})`;
    }, 1000);
}
