function suma(num1,num2) {
    if(isNaN(num1) || isNaN(num2)) {
        throw new Error('Alguno de los parametros no es un numero.');
    } else if(num1 < 0 || num2 < 0) {
        throw new Error('No se permiten numeros negativos.')
    } else {
        return num1 + num2;
    }
}


function resta(num1,num2) {
    if(isNaN(num1) || isNaN(num2)) {
        throw new Error('Alguno de los parametros no es un numero.');
    } else if(num1 < num2) {
        throw new Error('El resultado da un numero negativo.');
    } else if(num1 < 0 || num2 < 0) {
        throw new Error('No se permiten numeros negativos.')
    } else {
        return num1 - num2
    }
}


function multiplicar(num1,num2) {
    if(isNaN(num1) || isNaN(num2)) {
        throw new Error('Alguno de los parametros no es un numero.');
    } else if(num1 < 0 || num2 < 0) {
        throw new Error('No se permiten numeros negativos.')
    } else {
        return num1 * num2;
    }
}


function division(num1,num2) {
    if(isNaN(num1) || isNaN(num2)) {
        throw new Error('Alguno de los parametros no es un numero.');
    } else if(num1 === 0 || num2 === 0) {
            throw new Error('El numero no puede ser 0.')
    } else if(num1 < 0 || num2 < 0) {
        throw new Error('No se permiten numeros negativos.')
    } else {
        return num1 / num2;
    }
}

export { suma, resta, multiplicar, division };