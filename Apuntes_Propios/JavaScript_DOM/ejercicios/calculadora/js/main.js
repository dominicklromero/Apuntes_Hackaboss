import { suma, resta, multiplicar, division} from './functions.js';

const form = document.querySelector('div.cajaForm > form');
const inputValue1 = form.elements.value1;
const inputValue2 = form.elements.value2;
const divResultado = document.querySelector('div.resultFinal');

const calcular = (e) => {
    
    e.preventDefault();
    const {target} = e

    if(target.matches('button.calc')) {
        let resultado;

        const operacion = form.elements.operacion;

        if(inputValue1.value === '' || inputValue2.value === '' || operacion.value === '') {
            throw new Error('No ha puesto ningun numero.');
        } else if(operacion.value === 'suma') {
            resultado = suma(Number(inputValue1.value),Number(inputValue2.value));
        } else if(operacion.value === 'resta') {
            resultado = resta(Number(inputValue1.value),Number(inputValue2.value));
        } else if(operacion.value === 'multiplicacion') {
            resultado = multiplicar(Number(inputValue1.value),Number(inputValue2.value));
        } else if(operacion.value === 'division') {
            resultado = division(Number(inputValue1.value),Number(inputValue2.value));
        }
        divResultado.innerHTML = `<h1>${resultado}</h1>`;
    }
}

form.addEventListener('click', calcular);


const limpiar = (e) => {

    e.preventDefault();
    const {target} = e;

    if(target.matches('button.limpiar')) {
        inputValue1.value = '';
        inputValue2.value = '';

        const h1 = document.querySelector('div.resultFinal > h1');
        h1.remove();
    }
}

form.addEventListener('click', limpiar);