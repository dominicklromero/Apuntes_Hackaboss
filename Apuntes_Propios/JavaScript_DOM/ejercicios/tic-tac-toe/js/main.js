'use strict';

import State, { updateState, resetState, checkWinner } from './state.js';

// Seleccionamos el main.
const main = document.querySelector('main');

// Seleccionamos el tablero: div con clase "board".
const boardDiv = document.querySelector('div.board');

// Seleccionamos las tres lineas de cuadrados: los tres divs dentro del anterior.
const firstRowDiv = document.querySelector('div.first-row');
const secondRowDiv = document.querySelector('div.second-row');
const thirdRowDiv = document.querySelector('div.third-row');

/**
 * #######################
 * ## handleSquareClick ##
 * #######################
 *
 * Esta función manejadora dicta lo que sucede cada vez que un jugador
 * hace click en un cuadrado.
 *
 *  - Si el objetivo es el div con clase "square" comprobamos qué jugador
 *    está jugando: rondas pares "X" rondas impares "O".
 *
 *  - Posteriormente obtenemos el index del cuadrado sobre el que pulsamos.
 *
 *  - Actualizamos el tablero.
 *
 *  - Renderizamos los cambios en el HTML.
 *
 */
const handleSquareClick = (e) => {

    const {target} = e;

    if (target.matches('div.square')) {
        const index = Number(target.getAttribute('data-index'));
        if (State.round % 2 === 0 && State.board[index] === null) {
            updateState(index,'X');
        } else if (State.round % 2 !== 0 && State.board[index] === null){
            updateState(index,'O');
        }

        render();
    }
};

boardDiv.addEventListener('click',handleSquareClick);

// Agregamos el evento de click al div con clase "board".


/**
 * ######################
 * ## Resetear partida ##
 * ######################
 * 
 * Agregamos un manejador de evento al main que compruebe si el elemento 
 * clickado es el div con clase "reset". Si es así:
 * 
 *  - Eliminamos el elemento padre.
 * 
 *  - Reseteamos el tablero.
 * 
 *  - Activamos de nuevo la función manejadora "handleSquareClick"
 * 
 */

main.addEventListener('click', (e) => {

    const {target} = e;

    if (target.matches('div.reset > *')) {
        const parent = target.parentElement;
        parent.remove();

        resetState();
        boardDiv.addEventListener('click',handleSquareClick);
        render();
    }
});

/**
 * ############
 * ## Render ##
 * ############
 */

function render() {
    
    // Vaciamos las tres filas de casillas.

    // Creamos las filas. Para ello debemos recorrer el tablero (board).
    
    // Creamos un div y le agregamos el contenido de la posición actual 
    // del tablero. 
    
    // Agregamos al div la clase "square" y el atributo "data-index"con
    //  el valor del index actual.

    // Recuerda que el tablero tiene 9 elementos. Los tres primeros son
    // las casillas de la primera fila, los 3 siguientes las casillas de
    // la segunda fila, y los tres últimos son las casillas de la última
    // fila. Agrega las casillas como hijo de la fila correspondiente.

    // Almacenamos el valor que retorne la función winner.

    // Si hay un ganador...
  
    // Eliminamos el event listener que permite clickar en los divs, 
    // es decir, en cada casilla.
    
    // Creamos un div y le agregamos la clase "reset".
        
    // Agregamos al div un párrafo con un mensaje que indique el ganador
    // y un h2 que diga "Try againg".
    
    // Agregamos al main el div.

    firstRowDiv.innerHTML = '';
    secondRowDiv.innerHTML = '';
    thirdRowDiv.innerHTML = '';

    for (let i = 0; i < State.board.length; i++) {
        const div = document.createElement('div');
        div.textContent = State.board[i];
        div.classList.add('square');
        div.setAttribute('data-index', i);

        if ( i < 3) firstRowDiv.append(div);
        if ( i >= 3 && i < 6) secondRowDiv.append(div);
        if ( i >= 6 && i < 9) thirdRowDiv.append(div);
    }

    const resultado = checkWinner();


    const resetDiv = document.createElement('div');
    resetDiv.classList.add('reset');

    if (resultado) {
        boardDiv.removeEventListener('click', handleSquareClick);
        if (State.round % 2 === 0 && State.round <= 9) {
            resetDiv.innerHTML = 
            `
            <p>Ganador Player 2</p>
            <h2>Try again</h2>
            `;
        } else if (State.round % 2 !== 0 && State.round <= 9) {
            resetDiv.innerHTML = 
            `
            <p>Ganador Player 1</p>
            <h2>Try again</h2>
            `;
        }
    }
    main.append(resetDiv);
}

render();
