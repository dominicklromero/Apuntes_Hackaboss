/**
 * Crea un reloj que se actualice en tiempo real:
 *
 *  - La hora debe ir en el <h1>
 *  - La fecha debe ir en el <h2>
 *
 * En función de la hora del día la imagen de fondo debe cambiar.
 * Para este punto puedes ayudarte de las clases:
 *
 *  - morning: a partir de las 7:00.
 *
 *  - afternoon: a partir de las 13:00.
 *
 *  - night: a partir de las 21:00.
 *
 */

// Primera forma  -->  Con la funcion toLocaleDateString() o toLocaleTimeString()

/* function sacartime() {
    const now = new Date();
    const timeoptions = {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
    };
    const hora = now.toLocaleTimeString('es-ES', timeoptions);
    return hora;
}
function sacardate() {
    const now = new Date();
    const dateoptions = {
        day: 'numeric',
        year: 'numeric',
        month: 'long',
    };
    const fecha = now.toLocaleDateString('es-ES', dateoptions);
    return fecha;
}

const { body } = document;
const h1 = body.querySelector('h1');
const h2 = body.querySelector('h2');

setInterval(() => {
    h2.textContent = sacardate();
    h1.textContent = sacartime();
    const nowhour = new Date(); // Esto es para comprobar que cambia los fondos
    const hour = nowhour.getSeconds();
    body.classList.remove('morning', 'afternoon', 'night');
    if (hour >= 7 && hour < 13) {
        // body.classList.remove('afternoon');
        // body.classList.remove('night');
        body.classList.add('morning');
    } else if (hour >= 13 && hour < 21) {
        // body.classList.remove('morning');
        // body.classList.remove('night');
        body.classList.add('afternoon');
    } else {
        // body.classList.remove('morning');
        // body.classList.remove('afternoon');
        body.classList.add('night');
    }

    setTimeout(() => {
        h1.textContent = sacartime().replaceAll(':', ' ');
    }, 500);
}, 1000); */

// Segunda forma  -->  Con getDate, etc.

const { body } = document;

function nomMes(num) {
    const month = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];
    return month[num];
}

function fechaActual() {
    const current = new Date();
    const diaNum = current.getDate();
    const anhoNum = current.getFullYear();
    const nombreMes = nomMes(current.getMonth());
    return `${diaNum} de ${nombreMes} de ${anhoNum}`;
}

function horaActual() {
    const current = new Date();
    let hora = current.getHours();
    let min = current.getMinutes();
    let sec = current.getSeconds();

    if (hora < 10) {
        hora = '0' + hora;
    } else if (min < 10) {
        min = '0' + min;
    } else if (sec < 10) {
        sec = '0' + sec;
    }

    if (hora >= 7 && hora < 13) {
        body.classList.remove('afternoon', 'night');
        body.classList.add('morning');
    } else if (hora >= 13 && hora < 21) {
        body.classList.remove('morning', 'night');
        body.classList.add('afternoon');
    } else {
        body.classList.remove('morning', 'afternoon');
        body.classList.add('night');
    }

    return `${hora}:${min}:${sec}`;
}

setInterval(() => {
    const h1 = body.querySelector('h1');
    const h2 = body.querySelector('h2');

    h2.textContent = fechaActual();
    h1.textContent = horaActual();

    setTimeout(() => {
        h1.textContent = horaActual().replaceAll(':', ' ');
    }, 500);
}, 1000);
