'use strict';

// Selecciono el parrafo
const parrafo = document.querySelector('body > p');

//Selecciono el body
/* const body = document.body; */
// Por destructuring
/* const { body } = document; */

let contador = 0;

function random(num) {
    return Math.floor(Math.random() * (num + 1));
}

console.log(random(4));

setInterval(() => {
    document.body.style.backgroundColor = `rgb(${random(255)}, ${random(
        255
    )}, ${random(255)})`;

    parrafo.textContent = contador++;
}, 1000);
