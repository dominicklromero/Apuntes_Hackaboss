'use strict';

//Altura de la ventana en px
console.log(window.innerHeight);

//Anchura de la ventana en px
console.log(window.innerWidth);

//Altura de la ventana al completo en px
console.log(window.outerHeight);

//Altura de la ventana al completo en px
console.log(window.outerWidth);

//Mostrar el objeto location
console.log(window.location); // Asi se redirecciona a otra pagina
/* console.log((window.location.href = 'https://google.com')); */

//Para saber el navegador que esta usando el Usuario
console.log(window.navigator.userAgent);

const { userAgent } = window.navigator;
let userBrowser;

if (userAgent.indexOf('Chrome') > -1) {
    userBrowser = 'Google Chrome';
} else if (userAgent.indexOf('Safari') > -1) {
    userBrowser = 'Apple Safari';
} else if (userAgent.indexOf('Opera') > -1) {
    userBrowser = 'Opera';
} else if (userAgent.indexOf('Firefox') > -1) {
    userBrowser = 'Mozilla Firefox';
} else if (userAgent.indexOf('MSIE') > -1) {
    userBrowser = 'Microsoft Internet Explorer';
}

console.log(userBrowser);

//Mostar el scroll horizontal y vertical en px
console.log(window.scrollX, window.scrollY);

//Objeto console
console.log(window.console);

//Abrir una nueva pestaña
const google = window.open('https://google.com');

//Cerrar la pestaña anterior
setTimeout(() => google.close(), 3000);

//Abrir ventana de impresion. Pone el codigo de JS en espera hasta que cerremos la ventana de impresion
console.log(window.print());
