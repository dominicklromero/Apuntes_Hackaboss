'use strict';

//Seleccionando un nodo por ID
const secondLI = document.getElementById('Dos');

console.log(secondLI);

//Seleccionando un nodo de una forma mas guay
const body = document.querySelector('body'); //En querySelector se puede poner cualquier selector de CSS

console.log(body);

const header = document.querySelector('#principal');

console.log(header);

//Seleccionando varios nodos por clase
const claseElements = document.getElementsByClassName('miClase'); //Devuelve un array con getElementByClassName

console.log(claseElements);

for (const index of claseElements) {
    console.log(index); //Muestra cada elemento que tenga la clase 'miClase'
}

//Seleccionando por etiqueta
const allLI = document.getElementsByTagName('li');

console.log(allLI);

const allLI_2 = document.querySelectorAll('li'); //Mejor forma de hacerlo y devuelve un array tambien

console.log(allLI_2);

//Seleccionando el elemento padre HTML y moviendose por el DOM
const html = document.querySelector('html');

console.log(html.children); //El .children te muestra los hijos del nodo al que apunta

console.log(html.firstElementChild); //Muestra el primer hijo del nodo

console.log(html.lastElementChild); //Muestra el ultimo hijo del nodo

const miBody = html.lastElementChild; //body

console.log(miBody.previousElementSibling); //Muestra el hermano anterior. .nextElementSibling seleccionaria el hermano siguiente

console.log(miBody.parentElement); //Muestra el padre del nodo

//Modificando el texto de los elementos

secondLI.textContent = 'Adios'; //Otra forma
/* secondLI.textContent += ', Adios'; */ const liName = 'Uno';

/* body.innerHTML = `
    <header id="principal">
    <h1>DOM API</h1>
    </header>
    <main>
    <h2>Modificando movidas</h2>
    <ul class="miClase">
        <li>${liName}</li>
        <li id="Dos">Dos</li>
    </ul>
    <button type="button">¡Pulsame!</button>
    </main>`; */

//Modificando atributos de los elementos
const button = document.querySelector('button');

console.log(body.getAttribute('lang')); //Te devuelve el atributo si existe, si no existe devuelve NULL

console.log(button.setAttribute('disabled', true)); //Hay que pasarle dos parametros setAttribute(propiedad,valor)

console.log(button.hasAttribute('type')); //Comprobamos si tiene un atributo

console.log(button.removeAttribute('disabled')); //Borra atributos del nodo

//Modificando CSS
body.style.backgroundColor = 'lightCoral';

secondLI.style.cssText = `
    color:white;
    background-color: darkblue
`; //Modificas con texto CSS

secondLI.setAttribute('style', 'color: pink');

//Agregar una clase
body.classList.add('principal'); //se usa siempre .classList cuando manipulamos clases

//Borrar clase
body.classList.remove('miClase');

body.classList.toggle('principal'); //Si la clase que queremos agregar existe se la quita
body.classList.toggle('principal'); //Si la clase que queremos agregar no existe la agrega

console.log(body.classList.contains('principal')); // Devuelve true o false si tiene la clase o no

body.classList.replace('principal', 'secundario'); // Reemplaza una clase por otra

//Creando, borrando y sustituyendo elementos
const ul = document.querySelector('body > main > ul'); //Creamos un ul

const newLI = document.createElement('li');

newLI.textContent = 'Soy nuevo!!!'; //Agregamos texto a un nuevo li

ul.append(newLI); //Se agrega como ultimo hijo

ul.prepend(newLI); //Mueve de sitio a primer hijo, nunca crea otro nuevo

newLI.remove(); //Eliminamos el elemento

const colors = ['azul', 'rojo', 'amarillo'];

const frag = document.createDocumentFragment(); //Forma correcta de hacerlo

for (const color of colors) {
    const newLI_2 = document.createElement('li');
    newLI_2.textContent = color;
    frag.append(newLI_2);
}

ul.append(frag);

const contenedorRojo = document.querySelector('div');

console.log(contenedorRojo.getBoundingClientRect()); //Muestra las dimensiones del div
