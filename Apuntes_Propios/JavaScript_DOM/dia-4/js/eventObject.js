'use strict';

const button = document.querySelector('button');
const input = document.querySelector('form > input');
const { body } = document;

const handleClick = (event) => {
    console.log(event);

    console.log(event.target); //Apunta al elemento al cual hemos hecho click (Porque el evento es click)

    console.log(event.currentTarget); //Apunta al elemento al que le asignamos el evento (en este caso button)
};

body.addEventListener('click', handleClick);

input.addEventListener('keydown', (e) => {
    console.log(e.target.value);
});
