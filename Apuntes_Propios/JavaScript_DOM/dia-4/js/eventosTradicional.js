'use strict';

const button = document.querySelector('button');
const input = document.querySelector('form > input');

let count = 0;

button.onclick = function () {
    console.log(count++);
};

button.onmouseover = function () {
    console.log('Acabas de pasar el raton por encima');
};

input.onfocus = function () {
    console.log('Focus sobre el input');
};

/*
    Esta es la forma de detectar eventos antes de que existiese la funcion addEventListener
*/
