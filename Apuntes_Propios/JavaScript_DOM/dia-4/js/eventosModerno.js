'use strict';

const button = document.querySelector('button');
const input = document.querySelector('form > input');

//Primera Forma
button.addEventListener('click', function () {
    console.log('¡Click en el boton!'); //El addeventListener recibe 2 argumentos ('evento', callback).
});

//Segunda Forma
const handleButtonMouseOver = () => {
    console.log('Has pasado el raton por encima'); //Guardamos el callback en una variable, siempre que queramos deshabilitar una funcion manejadora.
};

button.addEventListener('mouseover', handleButtonMouseOver); //Y le pasamos el callback (guardado anteriormente) en el segundo argumento.

//Para eliminar un evento
setTimeout(() => {
    button.removeEventListener('mouseover', handleButtonMouseOver);
}, 10000);

//Se elimina con removeEventListener(). En este caso lo metemos en un timeout porque declaramos el evento arriba y lo quitamos despues.
//Solo se puede eliminar si el callback viene referenciado en una variable.
