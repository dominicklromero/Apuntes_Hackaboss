'use strict';

const now = new Date(); // Obtenemos la fecha actual. Si le pasamos en un string en formato timestamp te devuelve la fecha de la fecha dada como string.

console.log(now.getFullYear()); // Obtiene el año actual
console.log(now.getMonth() + 1); // Parte desde el mes 0
console.log(now.getDate()); // El dia actual
console.log(now.getDay()); // El dia actual de la semana
console.log(now.getHours()); // Horas
console.log(now.getMinutes()); // Minutos
console.log(now.getSeconds()); // Segundos
console.log(now.getMilliseconds()); // Milisegundos

const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
};
const formatDate = now.toLocaleDateString('es-ES', options); // Se le pasan como parametros el formato de la fecha y los valores en un objeto
console.log(formatDate);

setInterval(() => {
    const currentDate = new Date();

    const hour = currentDate.getHours();
    const minutes = currentDate.getMinutes();
    const sec = currentDate.getSeconds();

    console.log(`Hora actual --> ${hour} : ${minutes} : ${sec}`);
}, 1000);
